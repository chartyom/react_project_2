FROM node:8.9.1-alpine

ENV NODE_ENV=production 
ENV MONGO_HOST=127.0.0.1

# Copy application files
COPY ./ /app
WORKDIR /app

# RUN npm install --quite

CMD [ "npm", "start" ]