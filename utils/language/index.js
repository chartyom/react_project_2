const config = require('../../config')
const url = require('url')
// const i18n_module = require('i18n-nodejs')
const path = require("path")

// var i18nActive = true

// function I18n(lang) {
//     if (i18nActive) {
//         console.log("active i18n")
//         i18nActive = false
//         //i18n = new i18n_module(lang, path.join(__dirname, 'locale.json'))
//         // global.__ = i18n.__
//         console.log(global)
//     }
// }

/**
 * Check lang in array languages
 * @param {string} lang 
 * @param {array} arrayLang
 */
function LangIsExist(lang, arrayLang) {
    if (lang == undefined || !lang) return false
    return arrayLang.indexOf(lang) == -1 ? false : true
}

function HandleMiddleware(req, res, next) {
    req.cookies.lang = ReqGetLang(req)
    res.cookie('lang', req.cookies.lang, {
        expires: new Date(Date.now() + 9000000000)
    });
    next();
};

/**
 * 
 * @param {object} req 
 */
function ReqGetLang(req) {
    // [{key: "en", value: "English"}]
    var languages = config.settings.languages.map(function (v) { return v.key })

    var queryLang = req.query.lang
    if (LangIsExist(queryLang, languages)) {
        return queryLang
    }
    var cookielang = req.cookies.lang
    if (LangIsExist(cookielang, languages)) {
        return cookielang
    }

    // var { pathname } = url.parse(req.url)
    // // example: /en/link => ["", "en", "link"]
    // var u = pathname.split("/")[1];
    // if (LangIsExist(u, languages)) {
    //     return u
    // }

    return "en"
}


module.exports = {
    HandleMiddleware,
    ReqGetLang
}