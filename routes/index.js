const bodyParser = require('body-parser')
const htmlParser = bodyParser.text({ type: 'text/html' })
const config = require('../config')
const HandleMiddleware = require('../utils/language').HandleMiddleware
const Languages = require('../languages')

async function handlerGet(req, res, lang) {
    var {
        HeaderNavbar,
        ContentTeam,
        ContentIntroBlock,
        ContentDecisionImageBlock,
        ContentDecisionBlock,
        ContentMainAdvantages,
        ContentPreview,
        ContentTokenValue,
        ContentContacts,
        ContentRoadmap,
        ContentMedia,
        AccountForm
    } = Languages()[lang]
    global.LANGUAGE = lang
    const filesApp = await require('../config/' + lang + '.files.app.json')
    //const content = renderContent(req.url)
    res.render('main', {
        bodyClass: "Application",
        lang: lang,
        title: "Bitkom",
        applicationName: "Bitkom",
        files: {
            style: filesApp['app.css'],
            manifest: filesApp['manifest.app.js'],
            vendor: filesApp['vendor.app.js'],
            app: filesApp['app.js'],
        },
        INITIAL_STATE: JSON.stringify({
            HeaderNavbar,
            ContentTeam,
            ContentIntroBlock,
            ContentDecisionImageBlock,
            ContentDecisionBlock,
            ContentMainAdvantages,
            ContentPreview,
            ContentTokenValue,
            ContentContacts,
            ContentRoadmap,
            ContentMedia,
            AccountForm
        })
    });
}

module.exports = function (app) {
    config.settings.languages.map((e, i) => {
        app.get('/' + e.key + '*', htmlParser, HandleMiddleware, function (req, res) {
            return handlerGet(req, res, e.key).catch(e => console.log(e))
        });
    })

    app.get('*', htmlParser, HandleMiddleware, function (req, res) {
        return handlerGet(req, res, req.cookies.lang).catch(e => console.log(e))
    });

}

