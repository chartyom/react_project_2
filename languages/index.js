function Language() {
    return {
        en: {
            AccountForm: {
                title: "Bitkom account",
                description: "HOW TO BUY BTT?",
                addwallet: {
                    title: "ADD",

                },
                copywallet: {
                    title: "BUY",
                    content: "Go to your wallet account and send<br/> <span>founds to Bitkom ICO wallet!</span> <br/> BTT tokens will be transferred <span>ONLY</span> via Myetherwallet or Mist, do not use assets exchanges as Poloniex, Bittrex and wallets as Jaxx, Parity and others.",
                    description: "Bitkom Crowdsale smart contract address",
                },
                await: {
                    title: "RECEIVE",
                    content: "You will receive BTT during 15 minutes after purchase",
                    // description: "You will receive your BTT tokens on MyEtherWallet after stage is finished"
                },
                ethWallet: '0x6fC21092DA55B392b045eD78F4732bff3C580e2c',
                etcWallet: '0x6fC21092DA55B392b045eD78F4732bff3C580e2c',
                btcWallet: '0x6fC21092DA55B392b045eD78F4732bff3C580e2c',
                bchWallet: '',
                dashWallet: '',
                ltcWallet: '123'
            },
            HeaderNavbar: {
                Onepager: "https://drive.google.com/file/d/10ddsNsTd42cuxE8yP2qcMKcM8u5c61RJ/view?usp=sharing",
                Whitepaper: "https://drive.google.com/file/d/1LGilWbAZo_ogwkbz3gB3XMCHEhjBQl91/view?usp=sharing",
                TokenSaleTerms: "https://drive.google.com/open?id=1hmMfU6QbZW4tYa4Od_9BQMCCqd_zL_eM",
            },
            ContentTeam: {
                title: "Team",
                list: [
                    {
                        photo: "/images/team/2_c.png",
                        name: "Vladimir Nelub",
                        info: "Founder",
                        linkedin: "https://www.linkedin.com/in/vladimir-nelub-%D0%BD%D0%B5%D0%BB%D1%8E%D0%B1-bmstu-b1985257/",
                    },
                    {
                        photo: "/images/team/8_c.png",
                        name: "Oleg Nogin",
                        info: "CTO",
                        linkedin: "https://www.linkedin.com/in/oleg-nogin-ab457411b/",
                    },
                    {
                        photo: "/images/team/4_c.png",
                        name: "Alexander Khodko",
                        info: "Development Team Lead",
                        linkedin: "https://www.linkedin.com/in/alexander-khodko-1a8b88123/",
                    },
                    {
                        photo: "/images/team/6_c.png",
                        name: "Egor Shulgin",
                        info: "Android Developer",
                    },
                    {
                        photo: "/images/team/1_c.png",
                        name: "Artyom Chernyaev",
                        info: "Full Stack Developer",
                    },
                    {
                        photo: "/images/team/9_c.png",
                        name: "Margarita Stoyanova",
                        info: "Leading Business-Analyst",
                    },
                    {
                        photo: "/images/team/3_c.png",
                        name: "Elena Margulis",
                        info: "Head of the communication and sales division",
                        linkedin: "https://www.linkedin.com/in/elena-margulis-23b44241/",
                    },
                    {
                        photo: "/images/team/7_c.png",
                        name: "George Maslov",
                        info: "Economics analyst",
                    },
                    {
                        photo: "/images/team/5_c.png",
                        name: "Popova Irina",
                        info: "Analyst of the partnership division",
                    },
                    {
                        photo: "/images/team/10_c.png",
                        name: "Alexis Verne",
                        info: "Research and analytics",
                        linkedin: "https://www.linkedin.com/in/alexis-verne-619843131/",
                    },
                    {
                        photo: "/images/team/11_c.png",
                        name: "Johan Macq",
                        info: "Research and analytics",
                    }
                ]
            },
            ContentIntroBlock: {
                title: "Intro",
                content: "<p>The restaurant industry’s revenue is more than 2 700 billion USD. It is anticipated that it will be 3 805 billion USD by the end of 2019. Each month new establishments appear, and the existing ones either are reoriented or closed, which increases the demand for relevant information.</p>"
            },
            ContentDecisionImageBlock: {
                title: "Solution",
                image: "",
            },
            ContentDecisionBlock: {
                title: "Why the existing solutions are not the best?",
                content: "<div class='content'>" +
                    "<div class='content__article content__article--board1'><div class='content__article__text'>It is impossible to get up-to-date information about restaurants</div></div>" +
                    "<div class='content__article content__article--board2'><div class='content__article__text'>User prefers friend’s advice over some ‘magic’ behind his device</div></div>" +
                    "<div class='content__article content__article--board3'><div class='content__article__text'>None of the available recommendations services provides an all-in-one solution for both business and clients</div></div>" +
                    "</div>",
            },
            ContentMainAdvantages: {
                title: "Main advantages",
                business: {
                    title: "For business:",
                    content: "<p>1. Simple promotional and content management tools;</p>" +
                        "<p>2. Ready-to-use solution for cryptocurrency payment for restaurants.</p>",
                },
                users: {
                    title: "For customers:",
                    content: "<p>1. Restaurants’ related social network, where user can automatically follow his facebook friends or any other user, for example famous food blogger, and choose restaurants based on their opinions and experience.</p>" +
                        "<p>2. Maximizing coverage area: restaurant from any city can add itself to bitkom.io;</p>" +
                        "<p>3. Availability of in-app payments with both fiat and crypto currencies.</p>",
                }
            },
            ContentPreview: {
                title: "Bitkom",
                content: "<p>We are a restaurant recommendation services, located in Moscow, RU. We are running for more than 2 years and provide to our users:</p>" +
                    "<p>1. Restaurants recommendations using machine learning and neural networks;</p>" +
                    "<p>2. List and map of the nearby restaurants, selections made by «opinion leaders», restaurants news, discounts and other content;</p>" +
                    "<p>3. Account, referral system, social interaction, achievements and other ways of gamification.</p>",
                image: "/images/devices.jpg",
                appstore: "https://goo.gl/S6bpkS",
                googleplay: "https://goo.gl/EHVc25"
            },
            ContentMedia: {
                title: "Video",
                link: "https://www.youtube.com/embed/NOc3roL3xJo?rel=0"
            },
            ContentTokenValue: {
                title: "Token Value",
                info:
                    "<table>" +
                    "<tr>" +
                    "<td>Token Name:</td>" +
                    "<td>Bitkom Token</td>" +
                    "</tr>" +
                    "<tr>" +
                    "<td>Symbol:</td>" +
                    "<td>BTT</td>" +
                    "</tr>" +
                    "<tr>" +
                    "<td>Blockchain:</td>" +
                    "<td>Ethereum (ERC-20)</td>" +
                    "</tr>" +
                    "<tr>" +
                    "<td>Minimum amount of tokens sold to start a project (softcap):</td>" +
                    "<td>1 600 000 BTT</td>" +
                    "</tr>" +
                    "<tr>" +
                    "<td>Number of tokens in one hand:</td>" +
                    "<td>unlimited</td>" +
                    "</tr>" +
                    "<tr>" +
                    "<td>Minimum transaction amount in Ethereum:</td>" +
                    "<td>0.1 ETH</td>" +
                    "</tr>" +
                    "</table>",
                ico: {
                    title: "ICO",
                    content: "<table>" +
                        "<tr>" +
                        "<td>Beginning:</td>" +
                        "<td>30 December, 2017 (12:00 UTC)</td>" +
                        "</td>" +
                        "<tr>" +
                        "<td>End:</td>" +
                        "<td>30 January, 2018 (12:00 UTC)</td>" +
                        "</td>" +
                        "<tr>" +
                        "<td>Number of tokens for ICO:</td>" +
                        "<td>33 500 000 BTT</td>" +
                        "</td>" +
                        "<tr>" +
                        "<td>Exchange rate of tokens:</td>" +
                        "<td>1 ETH = 2 500 BTT</td>" +
                        "</td>" +
                        "</table>",
                },
                bonuses: {
                    title: "Bonuses",
                    content: "<table>" +
                        "<tr>" +
                        "<td colspan='2'>Amount of sold tokens</td>" +
                        "<td>Amount of bought tokens * bonus</td>" +
                        "</tr>" +
                        "<tr>" +
                        "<td>From [BTT]</td>" +
                        "<td>To [BTT]</td>" +
                        "<td>Bonus</td>" +
                        "</tr>" +
                        "<tr>" +
                        "<td>0</td>" +
                        "<td>10 000 000</td>" +
                        "<td>x4</td>" +
                        "</tr>" +
                        "<tr>" +
                        "<td>10 000 000</td>" +
                        "<td>20 000 000</td>" +
                        "<td>x3</td>" +
                        "</tr>" +
                        "<tr>" +
                        "<td>20 000 000</td>" +
                        "<td>30 000 000</td>" +
                        "<td>x2</td>" +
                        "</tr>" +
                        "<tr>" +
                        "<td>30 000 000</td>" +
                        "<td>33 500 000</td>" +
                        "<td>x1</td>" +
                        "</tr>" +
                        "</table>",
                },
                image: "/images/Funds_and_TokenEn.png",
            },
            ContentContacts: {
                title: "Contacts",
                content: "<p>For additional information relating to Bitkom, please visit our website bitkom.io and please contact us via our e-mail <a href='mailto:support@bitkom.io'>support@bitkom.io</a></p>" +
                    "<p>Official Twitter webpage: <a href='//twitter.com/bitkom_io'>twitter.com/bitkom_io</a></p>" +
                    "<p>Official Facebook webpage: <a href='//fb.me/bitkom.io'>fb.me/bitkom.io</a></p>" +
                    "<p>Official Instagram webpage: <a href='//instagram.com/bitkom.io'>instagram.com/bitkom.io</a></p>" +
                    "<p>Official Slack  webpage: <a href='//bitkomio.slack.com'>bitkomio.slack.com</a></p>" +
                    "<p>Official Telegram chat: <a href='//t.me/joinchat/B0PqQQ1MqSxesCkI5QLilw'>t.me/Bitkom.io</a></p>" +
                    "<p>iOS App Store: <a href='//goo.gl/S6bpkS'>Download</a></p>" +
                    "<p>Android Google Play: <a href='//goo.gl/EHVc25'>Download</a></p>",
            },
            ContentRoadmap: {
                title: "Roadmap",
                firstSteps: "/images/steps_first[en].png",
                secondSteps: "/images/steps_second[en].png",
            },
            AgreementContent: {
                title: "User agreement",
                content: "<p>User agreement</p>"
            },
            AccessCountries: ["Afghanistan", "Albania", "Algeria", "American Samoa", "Andorra", "Angola", "Anguilla", "Antarctica", "Antigua & Barbuda", "Argentina", "Armenia", "Aruba", "Ascension Island", "Australia", "Austria", "Azerbaijan", "Bahamas", "Bahrain", "Bangladesh", "Barbados", "Belarus", "Belgium", "Belize", "Benin", "Bermuda", "Bhutan", "Bolivia", "Bosnia & Herzegovina", "Botswana", "Brazil", "British Indian Ocean Territory", "British Virgin Islands", "Brunei", "Bulgaria", "Burkina Faso", "Burundi", "Cambodia", "Cameroon", "Canary Islands", "Cape Verde", "Caribbean Netherlands", "Cayman Islands", "Central African Republic", "Ceuta & Melilla", "Chad", "Chile", "China", "Christmas Island", "Cocos (Keeling) Islands", "Colombia", "Comoros", "Congo - Brazzaville", "Congo - Kinshasa", "Cook Islands", "Costa Rica", "Croatia", "Cuba", "Cyprus", "Czech Republic", "Denmark", "Diego Garcia", "Djibouti", "Dominica", "Dominican Republic", "Ecuador", "Egypt", "El Salvador", "Equatorial Guinea", "Eritrea", "Estonia", "Ethiopia", "Falkland Islands", "Faroe Islands", "Fiji", "Finland", "France", "French Guiana", "French Polynesia", "French Southern Territories", "Gabon", "Gambia", "Georgia", "Germany", "Ghana", "Gibraltar", "Greece", "Greenland", "Grenada", "Guadeloupe", "Guam", "Guatemala", "Guernsey", "Guinea", "Guinea-Bissau", "Guyana", "Haiti", "Honduras", "Hungary", "Iceland", "India", "Indonesia", "Iran", "Iraq", "Ireland", "Isle of Man", "Israel", "Italy", "Jamaica", "Japan", "Jersey", "Jordan", "Kazakhstan", "Kenya", "Kiribati", "Kosovo", "Kuwait", "Kyrgyzstan", "Laos", "Latvia", "Lebanon", "Lesotho", "Liberia", "Libya", "Liechtenstein", "Lithuania", "Luxembourg", "Macau SAR China", "Macedonia", "Madagascar", "Malawi", "Malaysia", "Maldives", "Mali", "Malta", "Marshall Islands", "Martinique", "Mauritania", "Mauritius", "Mayotte", "Mexico", "Micronesia", "Moldova", "Monaco", "Mongolia", "Montenegro", "Montserrat", "Morocco", "Mozambique", "Myanmar (Burma)", "Namibia", "Nauru", "Nepal", "Netherlands", "New Caledonia", "New Zealand", "Nicaragua", "Niger", "Nigeria", "Niue", "Norfolk Island", "North Korea", "Northern Mariana Islands", "Norway", "Oman", "Pakistan", "Palau", "Palestinian Territories", "Panama", "Papua New Guinea", "Paraguay", "Peru", "Philippines", "Pitcairn Islands", "Poland", "Portugal", "Puerto Rico", "Qatar", "Romania", "Russia", "Rwanda", "Samoa", "San Marino", "Saudi Arabia", "Senegal", "Serbia", "Seychelles", "Sierra Leone", "Sint Maarten", "Slovakia", "Slovenia", "Solomon Islands", "Somalia", "South Africa", "South Georgia & South Sandwich Islands", "South Korea", "South Sudan", "Spain", "Sri Lanka", "St. Helena", "St. Kitts & Nevis", "St. Lucia", "St. Martin", "St. Pierre & Miquelon", "St. Vincent & Grenadines", "Sudan", "Suriname", "Svalbard & Jan Mayen", "Swaziland", "Sweden", "Switzerland", "Syria", "Taiwan", "Tajikistan", "Tanzania", "Thailand", "Timor-Leste", "Togo", "Tokelau", "Tonga", "Trinidad & Tobago", "Tristan da Cunha", "Tunisia", "Turkey", "Turkmenistan", "Turks & Caicos Islands", "Tuvalu", "U.S. Outlying Islands", "U.S. Virgin Islands", "Uganda", "Ukraine", "United Arab Emirates", "Uruguay", "Uzbekistan", "Vanuatu", "Vatican City", "Venezuela", "Vietnam", "Wallis & Futuna", "Western Sahara", "Yemen", "Zambia", "Zimbabwe"]
        },
        ru: {
            AccountForm: {
                title: "Bitkom account",
                description: "HOW TO BUY BTT?",
                addwallet: {
                    title: "ADD",

                },
                copywallet: {
                    title: "BUY",
                    content: "Go to your wallet account and send<br/> <span>founds to Bitkom ICO wallet!</span> <br/> BTT tokens will be transferred <span>ONLY</span> via Myetherwallet or Mist, do not use assets exchanges as Poloniex, Bittrex and wallets as Jaxx, Parity and others.",
                    description: "Bitkom Crowdsale smart contract address",
                },
                await: {
                    title: "RECEIVE",
                    content: "You will receive BTT during 15 minutes after purchase",
                    // description: "You will receive your BTT tokens on MyEtherWallet after stage is finished"
                },
                ethWallet: '0x6fC21092DA55B392b045eD78F4732bff3C580e2c',
                etcWallet: '0x6fC21092DA55B392b045eD78F4732bff3C580e2c',
                btcWallet: '0x6fC21092DA55B392b045eD78F4732bff3C580e2c',
                bchWallet: '',
                dashWallet: '',
                ltcWallet: '123'
            },
            HeaderNavbar: {
                Onepager: "https://drive.google.com/file/d/1EvXiuGzsCCYEOwKEtm0ahUJ7PLn0mvCp/view?usp=sharing",
                Whitepaper: "https://drive.google.com/file/d/165Do1xGyfkomLI9LZLrWmZUO7fqxOFIt/view?usp=sharing",
                TokenSaleTerms: "https://drive.google.com/open?id=1TkaPep2_-nqT8Y1DpZWBQezqOjIpmTrt",
            },
            ContentTeam: {
                title: "КОМАНДА",
                list: [
                    {
                        photo: "/images/team/2_c.png",
                        name: "Владимир Нелюб",
                        info: "Founder",
                        linkedin: "https://www.linkedin.com/in/vladimir-nelub-%D0%BD%D0%B5%D0%BB%D1%8E%D0%B1-bmstu-b1985257/",
                    },
                    {
                        photo: "/images/team/8_c.png",
                        name: "Олег Ногин",
                        info: "CTO",
                        linkedin: "https://www.linkedin.com/in/oleg-nogin-ab457411b/",
                    },
                    {
                        photo: "/images/team/4_c.png",
                        name: "Александр Ходько",
                        info: "Development Team Lead",
                        linkedin: "https://www.linkedin.com/in/alexander-khodko-1a8b88123/",
                    },
                    {
                        photo: "/images/team/6_c.png",
                        name: "Егор Шульгин",
                        info: "Android Developer",
                    },
                    {
                        photo: "/images/team/1_c.png",
                        name: "Артём Черняев",
                        info: "Full Stack Developer",
                    },
                    {
                        photo: "/images/team/9_c.png",
                        name: "Маргарита Стоянова",
                        info: "Ведущий бизнес-аналитик",
                    },
                    {
                        photo: "/images/team/3_c.png",
                        name: "Маргулис Елена",
                        info: "Руководитель отделам взаимодействия и продаж",
                        linkedin: "https://www.linkedin.com/in/elena-margulis-23b44241/",
                    },
                    {
                        photo: "/images/team/7_c.png",
                        name: "Маслов Георгий",
                        info: "Аналитик экономист",
                    },
                    {
                        photo: "/images/team/5_c.png",
                        name: "Попова Ирина",
                        info: "Специалист по работе с партнерамиж",
                    },
                    {
                        photo: "/images/team/10_c.png",
                        name: "Alexis Verne",
                        info: "Исследователь и аналитик",
                    },
                    {
                        photo: "/images/team/11_c.png",
                        name: "Johan Macq",
                        info: "Исследователь и аналитик",
                    }
                ]
            },
            // ContentСapability: {
            //     title: "BITKOM - БИЗНЕС РЕШЕНИЕ",
            //     list: [
            //         {
            //             title: "Простой инструмент управления контентом",
            //             content: "Благодаря наличию личного кабинета, ресторан или иной поставщик услуг получит возможность управлять информацией о себе, акциях и иных предложениях в одном месте, а благодаря наличию открытого API, каждый агрегатор сможет получать актуальную информацию, что повысит конкуренцию на рынке рекомендательных сервисов.",
            //             image: ""
            //         },
            //         {
            //             title: "Простой инструмент продвижением",
            //             content: "В этом же личном кабинете поставщик услуг сможет продвигать свое заведение или конкретное предложение в приложениях bitkom.io без подписания каких-либо договоров, сможет оплачивать маркетинговые активности как деньгами, так и нашей криптовалютой, а также управлять своими криптосредствами и принимать оплату в нашей криптовалюте от пользователя.",
            //             image: ""
            //         },
            //     ]
            // },
            // ContentInformationBlock: {
            //     title: "В БУДУЩЕМ",
            //     content: "Our vision is to revolutionize the way musicians are presented and booked as well as to open the world of live music to anyone who wants to participate in it by decentralizing discovery and booking processes with the use of the blockchain technology."
            // },
            ContentMedia: {
                title: "Видео",
                link: "https://www.youtube.com/embed/NOc3roL3xJo?rel=0"
            },
            ContentIntroBlock: {
                title: "Проблема",
                content: "<p>Ресторанная индустрия имеет доход более 2 700 млрд. долларов США. Ожидается, что он приблизится к 3 805 млрд. долларов США к концу 2019. Каждый месяц появляются новые заведения, а существующие переориентируются или закрываются, что повышает востребованность актуальной информации.</p>"
            },
            ContentDecisionImageBlock: {
                title: "Решение",
                image: "",
            },
            ContentDecisionBlock: {
                title: "Почему существующие решения не оптимальны?",
                content: "<div class='content'>" +
                    "<div class='content__article content__article--board1'><div class='content__article__text'>Невозможно поддерживать актуальность информации о ресторанах</div></div>" +
                    "<div class='content__article content__article--board2'><div class='content__article__text'>Пользователь предпочитает советы друзей, а не «магию» внутри устройства</div></div>" +
                    "<div class='content__article content__article--board3'><div class='content__article__text'>Ни один из существующих рекомендательных сервисов не предлагает готового решения как для пользователя, так и для бизнеса</div></div>" +
                    "</div>",
            },
            ContentMainAdvantages: {
                title: "Основные преимущества",
                business: {
                    title: "Для бизнеса:",
                    content: "<p>1. Удобное средство продвижения и управления контентом;</p>" +
                        "<p>2. Готовое решение для внедрения приема оплаты криптовалютой.</p>",
                },
                users: {
                    title: "Для пользователей:",
                    content: "<p>1. Социальная сеть о ресторанах, где пользователь может следить за друзьями из facebook или другими пользователями системы, например известными блоггерами, и выбирать ресторан основываясь на их мнение;</p>" +
                        "<p>2. Максимизация зоны охвата: ресторан из любого города может внести себя в систему;</p>" +
                        "<p>3. Возможность платить внутри приложения как деньгами, так и криптовалютами.</p>",
                }
            },
            ContentPreview: {
                title: "Bitkom",
                content: "<p>Мы – рекомендательный сервис по подбору ресторанов, работающий в Москве более 2-х лет. Мы предлагаем пользователям:</p>" +
                    "<p>1. Рекомендации на основе машинного обучения и нейронных сетей;</p>" +
                    "<p>2. Списки и карту со всеми ресторанами, подборки от «лидеров мнений», новости, акции и другой контент;</p>" +
                    "<p>3. Аккаунт, реферальная система, социальное взаимодействие, достижения и другие способы геймификации.</p",
                image: "/images/devices.jpg",
                appstore: "https://goo.gl/S6bpkS",
                googleplay: "https://goo.gl/EHVc25"
            },
            ContentTokenValue: {
                title: "ICO",
                info:
                    "<table>" +
                    "<tr>" +
                    "<td>Название токена:</td>" +
                    "<td>Bitkom Token</td>" +
                    "</tr>" +
                    "<tr>" +
                    "<td>Символ:</td>" +
                    "<td>BTT</td>" +
                    "</tr>" +
                    "<tr>" +
                    "<td>Блокчейн:</td>" +
                    "<td>Ethereum (ERC-20)</td>" +
                    "</tr>" +
                    "<tr>" +
                    "<td>Минимальное кол-во проданных токенов для запуска проекта (softcap):</td>" +
                    "<td>1 600 000 BTT</td>" +
                    "</tr>" +
                    "<tr>" +
                    "<td>Количество токенов в одни руки:</td>" +
                    "<td>не ограничено</td>" +
                    "</tr>" +
                    "<tr>" +
                    "<td>Минимальная сумма транзакции в Ethereum:</td>" +
                    "<td>0.1 ETH</td>" +
                    "</tr>" +
                    "</table>",
                ico: {
                    title: "ICO",
                    content: "<table>" +
                        "<tr>" +
                        "<td>Начало:</td>" +
                        "<td>30 Декабря, 2017 (12:00 UTC)</td>" +
                        "</tr>" +
                        "<tr>" +
                        "<td>Конец:</td>" +
                        "<td>30 Января, 2018 (12:00 UTC)</td>" +
                        "</tr>" +
                        "<tr>" +
                        "<td>Количество токенов для ICO:</td>" +
                        "<td>33 500 000 BTT</td>" +
                        "</tr>" +
                        "<tr>" +
                        "<td>Курс обмена токенов:</td>" +
                        "<td>1 ETH = 2 500 BTT</td>" +
                        "</tr>" +
                        "</table>",
                },
                bonuses: {
                    title: "Бонусы",
                    content: "<table>" +
                        "<tr>" +
                        "<td colspan='2'>Количество проданных токенов</td>" +
                        "<td>Кол-во купленных токенов * Бонус</td>" +
                        "</tr>" +
                        "<tr>" +
                        "<td>От [BTT]</td>" +
                        "<td>До [BTT]</td>" +
                        "<td>Бонус</td>" +
                        "</tr>" +
                        "<tr>" +
                        "<td>0</td>" +
                        "<td>10 000 000</td>" +
                        "<td>x4</td>" +
                        "</tr>" +
                        "<tr>" +
                        "<td>10 000 000</td>" +
                        "<td>20 000 000</td>" +
                        "<td>x3</td>" +
                        "</tr>" +
                        "<tr>" +
                        "<td>20 000 000</td>" +
                        "<td>30 000 000</td>" +
                        "<td>x2</td>" +
                        "</tr>" +
                        "<tr>" +
                        "<td>30 000 000</td>" +
                        "<td>33 500 000</td>" +
                        "<td>x1</td>" +
                        "</tr>" +
                        "</table>",
                },
                image: "/images/Funds_and_TokenRu.png",
            },
            ContentContacts: {
                title: "Контактная информация",
                content: "<p>Для получения дополнительной информации, напишите нам на электронную почту <a href='mailto:support@bitkom.io'>support@bitkom.io</a></p>" +
                    "<p>Twitter: <a href='//twitter.com/bitkom_io'>twitter.com/bitkom_io</a></p>" +
                    "<p>Facebook: <a href='//fb.me/bitkom.io'>fb.me/bitkom.io</a></p>" +
                    "<p>Instagram: <a href='//instagram.com/bitkom.io'>instagram.com/bitkom.io</a></p>" +
                    "<p>Slack: <a href='//bitkomio.slack.com'>bitkomio.slack.com</a></p>" +
                    "<p>Telegram: <a href='//t.me/joinchat/B0PqQQ1MqSxesCkI5QLilw'>t.me/Bitkom.io</a></p>" +
                    "<p>iOS App Store: <a href='//goo.gl/S6bpkS'>Скачать</a></p>" +
                    "<p>Android Google Play: <a href='//goo.gl/EHVc25'>Скачать</a></p>",
            },
            ContentRoadmap: {
                title: "Карта развития",
                firstSteps: "/images/steps_first[ru].png",
                secondSteps: "/images/steps_second[ru].png",
            },
            AgreementContent: {
                title: "Пользовательское соглашение",
                content: "<p>Пользовательское соглашение</p>"
            },
            AccessCountries: ["Австралия", "Австрия", "Азербайджан", "Аландские о-ва", "Албания", "Алжир", "Американское Самоа", "Ангилья", "Ангола", "Андорра", "Антарктида", "Антигуа и Барбуда", "Аргентина", "Армения", "Аруба", "Афганистан", "Багамские о-ва", "Бангладеш", "Барбадос", "Бахрейн", "Беларусь", "Белиз", "Бельгия", "Бенин", "Бермудские о-ва", "Болгария", "Боливия", "Бонэйр, Синт-Эстатиус и Саба", "Босния и Герцеговина", "Ботсвана", "Бразилия", "Британская территория в Индийском океане", "Бруней-Даруссалам", "Буркина-Фасо", "Бурунди", "Бутан", "Вануату", "Ватикан", "Венгрия", "Венесуэла", "Виргинские о-ва (Британские)", "Виргинские о-ва (США)", "Внешние малые о-ва (США)", "Восточный Тимор", "Вьетнам", "Габон", "Гаити", "Гайана", "Гамбия", "Гана", "Гваделупа", "Гватемала", "Гвинея", "Гвинея-Бисау", "Германия", "Гернси", "Гибралтар", "Гондурас", "Гренада", "Гренландия", "Греция", "Грузия", "Гуам", "Дания", "Джерси", "Джибути", "Диего-Гарсия", "Доминика", "Доминиканская Республика", "Египет", "Замбия", "Западная Сахара", "Зимбабве", "Израиль", "Индия", "Индонезия", "Иордания", "Ирак", "Иран", "Ирландия", "Исландия", "Испания", "Италия", "Йемен", "Кабо-Верде", "Казахстан", "Каймановы о-ва", "Камбоджа", "Камерун", "Канарские о-ва", "Катар", "Кения", "Кипр", "Киргизия", "Кирибати", "Китай", "КНДР", "Кокосовые о-ва", "Колумбия", "Коморские о-ва", "Конго - Браззавиль", "Конго - Киншаса", "Косово", "Коста-Рика", "Кот-д’Ивуар", "Куба", "Кувейт", "Кюрасао", "Лаос", "Латвия", "Лесото", "Либерия", "Ливан", "Ливия", "Литва", "Лихтенштейн", "Люксембург", "Маврикий", "Мавритания", "Мадагаскар", "Майотта", "Макао (особый район)", "Македония", "Малави", "Малайзия", "Мали", "Мальдивские о-ва", "Мальта", "Марокко", "Мартиника", "Маршалловы о-ва", "Мексика", "Мозамбик", "Молдова", "Монако", "Монголия", "Монтсеррат", "Мьянма (Бирма)", "Намибия", "Науру", "Непал", "Нигер", "Нигерия", "Нидерланды", "Никарагуа", "Ниуэ", "Новая Зеландия", "Новая Каледония", "Норвегия", "о-в Вознесения", "О-в Мэн", "о-в Норфолк", "о-в Рождества", "О-в Св. Елены", "о-ва Кука", "О-ва Тёркс и Кайкос", "ОАЭ", "Оман", "Пакистан", "Палау", "Палестинские территории", "Панама", "Папуа – Новая Гвинея", "Парагвай", "Перу", "Питкэрн", "Польша", "Португалия", "Пуэрто-Рико", "Республика Корея", "Реюньон", "Россия", "Руанда", "Румыния", "Сальвадор", "Самоа", "Сан-Марино", "Сан-Томе и Принсипи", "Саудовская Аравия", "Свазиленд", "Северные Марианские о-ва", "Сейшельские о-ва", "Сен-Бартельми", "Сен-Мартен", "Сен-Пьер и Микелон", "Сенегал", "Сент-Винсент и Гренадины", "Сент-Китс и Невис", "Сент-Люсия", "Сербия", "Сеута и Мелилья", "Синт-Мартен", "Сирия", "Словакия", "Словения", "Соломоновы о-ва", "Сомали", "Судан", "Суринам", "Сьерра-Леоне", "Таджикистан", "Таиланд", "Тайвань", "Танзания", "Того", "Токелау", "Тонга", "Тринидад и Тобаго", "Тристан-да-Кунья", "Тувалу", "Тунис", "Туркменистан", "Турция", "Уганда", "Узбекистан", "Украина", "Уоллис и Футуна", "Уругвай", "Фарерские о-ва", "Федеративные Штаты Микронезии", "Фиджи", "Филиппины", "Финляндия", "Фолклендские о-ва", "Франция", "Французская Гвиана", "Французская Полинезия", "Французские Южные Территории", "Хорватия", "ЦАР", "Чад", "Черногория", "Чехия", "Чили", "Швейцария", "Швеция", "Шпицберген и Ян-Майен", "Шри-Ланка", "Эквадор", "Экваториальная Гвинея", "Эритрея", "Эстония", "Эфиопия", "ЮАР", "Южная Георгия и Южные Сандвичевы о-ва", "Южный Судан", "Ямайка", "Япония"]
        }
    }
}

module.exports = Language
