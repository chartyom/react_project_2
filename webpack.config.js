global.DEVELOPMENT = process.env.NODE_ENV != 'production' ? true : false;
global.ENVIRONMENT = process.env.NODE_ENV || 'local';

const webpack = require('webpack');
const path = require("path");
const config = require("./config");

const ExtractTextPlugin = require('extract-text-webpack-plugin');
const ManifestPlugin = require('webpack-manifest-plugin');
const I18nPlugin = require("i18n-webpack-plugin");
const autoprefixer = require('autoprefixer');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin')

const PUBLIC_SOURCE_PATH = path.join(__dirname, 'public');
const SOURCE_PATH = path.join(__dirname, 'src');
const BUILD_PATH = path.join(__dirname, 'dist');

//const CompressionPlugin = require("compression-webpack-plugin");

StartWithLog();

const I18N_LANGUAGES = {
    "en": null,
    "ru": require("./webpack_languages/ru.json")
};

const LANGUAGES = config.settings.languages

var Exports = MainMultilanguageApp()

module.exports = Exports;


function MainMultilanguageApp() {
    return Object.keys(I18N_LANGUAGES).map(function (language) {
        return {
            entry: {
                'vendor.app': [
                    'react',
                    'react-dom',
                    'react-router-dom',
                    'react-redux',
                    'redux-thunk',
                    'redux',
                    'isomorphic-fetch',
                    'es6-promise',
                    'react-helmet',
                    'react-modal'
                ],
                app: './src/Application.jsx',
            },
            output: {
                path: path.resolve(__dirname, 'public/temp'),
                filename: DEVELOPMENT ? language + '.[name].js' : language + '.[name].[chunkhash:5].js',
                publicPath: "js/", // string
            },
            // автозапуск после внесения изменений
            watch: DEVELOPMENT ? true : false,
            watchOptions: {
                // задержка в мс запуска программы после сохранения файла с изменениями
                aggregateTimeout: 600,
            },
            // различные способы проверки (debug) кода
            devtool: DEVELOPMENT ? 'source-map' : false,
            // Поиск require модулей 
            resolve: {
                modules: [
                    'node_modules',
                    SOURCE_PATH
                ],
                //extensions: ['', '.js', '.jsx', '.css', '.styl']
                extensions: ['.jsx', ".js", ".json"],
            },
            // Поиск loader модулей
            // resolveLoader: {
            //     modulesDirectories: ['node_modules'],
            //     moduleTemplates: ['*-loader', '*'],
            //     extensions: ['', '.js'],
            // },
            plugins: [
                new I18nPlugin(
                    I18N_LANGUAGES[language]
                ),
                //добавляет глобальные переменные в клиентскую часть
                new webpack.DefinePlugin({
                    ENVIRONMENT: JSON.stringify(ENVIRONMENT),
                    LANGUAGE: JSON.stringify(language),
                    LANGUAGES: JSON.stringify(LANGUAGES),
                    DEVELOPMENT: DEVELOPMENT,
                    'process.env': {
                        NODE_ENV: JSON.stringify(ENVIRONMENT)
                    }
                }),
                new webpack.optimize.ModuleConcatenationPlugin(),
                // extract vendor and webpack's module manifest
                new webpack.optimize.CommonsChunkPlugin({
                    names: ['vendor.app', 'manifest.app'],
                    minChunks: Infinity
                }),
                // extract common modules from all the chunks (requires no 'name' property)
                new webpack.optimize.CommonsChunkPlugin({
                    async: true,
                    children: true,
                    minChunks: 4
                }),
                //Файл manifest.json
                new ManifestPlugin({
                    fileName: '../../config/' + language + '.files.app.json',
                    publicPath: 'temp/'
                }),
                //Подключение глобальных переменных для jquery
                // new webpack.ProvidePlugin({
                //     //$: "jquery/dist/jquery.slim.min",
                //     //jQuery: "jquery/dist/jquery.slim.min"
                //     //React: "react/dist/react.min"
                // }),
                //Минимизирует код
                //new webpack.IgnorePlugin(/^\.\/locale$/, /moment$/),
                // new webpack.LoaderOptionsPlugin({
                //     minimize: true,
                //     debug: false
                // }),
                new webpack.LoaderOptionsPlugin({
                    minimize: true,
                    debug: DEVELOPMENT ? true : false,
                }),
                new UglifyJsPlugin({
                    parallel: true,
                    sourceMap: true,
                    uglifyOptions: {
                        compress: DEVELOPMENT ? false : true,
                        warnings: DEVELOPMENT ? true : false,
                    },
                }),

                new ExtractTextPlugin({
                    filename: DEVELOPMENT ? 'css/[name].css' : 'css/[name].[contenthash:5].css',
                    allChunks: true,
                }),
                // new CompressionPlugin({
                //     asset: "[path].gz[query]",
                //     algorithm: "gzip",
                //     test: /\.(js)$/,
                //     threshold: 10240,
                //     minRatio: 0.8
                // })
            ],
            module: {
                rules: [
                    // rules for modules (configure loaders, parser options, etc.)
                    {
                        test: /\.jsx?$/,
                        // include: [
                        //     path.resolve(process.cwd(), 'src/components')
                        // ],
                        exclude: /(node_modules)/,
                        loader: 'babel-loader',
                        query: {
                            presets: ["es2015", "stage-0", "react"]
                        }
                    },
                    {
                        test: /\.scss$/,
                        loader: ExtractTextPlugin.extract({
                            use: [
                                {
                                    loader: 'css-loader',
                                    options: {
                                        sourceMap: true
                                    }
                                },
                                {
                                    loader: 'postcss-loader',
                                    options: {
                                        plugins: [
                                            autoprefixer({
                                                browsers: ['ie >= 10', 'last 4 version']
                                            })
                                        ],
                                        sourceMap: true
                                    }
                                },
                                {
                                    loader: 'sass-loader',
                                    options: {
                                        sourceMap: true
                                    }
                                }
                            ],
                            fallback: "style-loader"
                        }),
                    },
                    {
                        test: /\.(jpe?g|png|gif|svg)$/i,
                        loaders: [
                            {
                                loader: 'file-loader',
                                options: {
                                    //useRelativePath: process.env.NODE_ENV === "production",
                                    name: '[name].[ext]?[hash:5]',
                                    outputPath: 'images/',
                                    publicPath: '/temp/',
                                }
                            },
                            // {
                            //     loader: 'image-webpack-loader',
                            //     options: {
                            //         pngquant: {
                            //             quality: '65-90',
                            //             speed: 3
                            //         },
                            //         optipng: {
                            //             optimizationLevel: 5,
                            //             //enabled: false,
                            //         },
                            //         gifsicle: {
                            //             interlaced: false,
                            //         },
                            //         mozjpeg: {
                            //             //progressive: true,
                            //             quality: 65
                            //         },
                            //         webp: {
                            //             quality: 75
                            //         },
                            //     }
                            // },
                        ]
                    },
                    {
                        test: /\.(eot?.+|svg?.+|ttf?.+|otf?.+|woff?.+|woff2?.+)$/,
                        loader: 'file-loader',
                        query: {
                            name: '[name].[ext]?[hash:5]',
                            outputPath: 'fonts/',
                            publicPath: '/temp/',
                        }
                    },
                    // {
                    //     test: /\.(png|gif|jpg|svg)$/,
                    //     use: [
                    //         {
                    //             loader: 'url-loader',
                    //             query: {
                    //                 limit: 10000,
                    //                 name: "/temp/[name].[ext]?[hash:5]",
                    //             }
                    //         }
                    //     ],
                    //     include: SOURCE_PATH
                    // }
                ]
            }
        }
    })
}

function StartWithLog() {
    console.log('==================');
    console.log('Webpack start with NODE_ENV:', ENVIRONMENT);
    console.log('==================');
}
