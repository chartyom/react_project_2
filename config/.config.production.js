const MONGO_HOST = process.env.MONGO_HOST

module.exports = {
    url: "http://localhost:8040",
    host: "127.0.0.1",
    port: "8040",
    mongo: {
        // url: `mongodb://${MONGO_HOST}:27017/accountapp`
        url: 'mongodb://user:account@ds133077.mlab.com:33077/accountappprod'
    },
    oauth: {
        facebook: {
            clientId: '130557234286607',
            clientSecret: '7c1f48c48067a81067723f38e0083adf'
        }
    },
    hostname: 'https://bitkom.io/'
}