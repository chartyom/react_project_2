module.exports = {
    url: "http://localhost:8040",
    host: "localhost",
    port: "8040",
    mongo: {
        url: 'mongodb://mongo/accountapp'
    },
    oauth: {
        facebook: {
            clientId: '130557234286607',
            clientSecret: '7c1f48c48067a81067723f38e0083adf'
        }
    },
    hostname: 'https://account.bitkom.io/'
}