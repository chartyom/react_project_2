import React from 'react'
import Helmet from 'react-helmet'
import { Switch, Route } from 'react-router-dom'
import { RouteAuth, RouteUnauth } from '../utils/Auth'
import { renderRoutes } from 'react-router-config'
import ThemeMain from "../layouts/ThemeMain"

import NotFoundPage from '../pages/NotFoundPage'
import HomePage from '../pages/HomePage'
import TokenValuePage from '../pages/TokenValuePage'
import RoadmapPage from '../pages/RoadmapPage'
import ContactsPage from '../pages/ContactsPage'
import SignInPage from '../pages/SignInPage'
import SignUpPage from '../pages/RegistrationPage'
import Url from '../url'
import routes from "../Routes"
var App = () => (
    <div className="App">
        <Helmet
            htmlAttributes={{ lang: LANGUAGE }}
            title="Bitkom"
            titleTemplate="%s - Blockchain-platform for catering and restaurant business."
        />
        {renderRoutes(routes)}
    </div>
)
export default App
