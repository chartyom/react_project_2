import React, { Component } from 'react'
import { renderRoutes } from 'react-router-config'
import { HeaderNavbar } from '../components/HeaderNavbar'
import { FooterMain } from '../components/FooterMain'
import { connect } from 'react-redux';
import * as Auth from '../utils/Auth'
import * as UserAction from '../actions/userAction'

class ThemeMain extends Component {
    componentDidMount() {
        this.Authorization()
    }

    Authorization() {
        var props = this.props
        if (props.user) {
            if (Auth.IsAuthenticated() && props.user.readyState != UserAction.USER_AUTHENTICATED) {
                if (DEVELOPMENT) console.log("Authenticated")
                UserAction.Get().then((res) => {
                    if (!res || res.error) {
                        return console.log(res.error);
                    }
                    if (!res.ok) {
                        return console.log("Error");
                    }
                    props.dispatch(UserAction.Auth(res.user));
                }).catch((e) => {
                    return console.log(e);
                })
            } else if (props.user.readyState != UserAction.USER_UNAUTHENTICATED) {
                props.dispatch(UserAction.Unauth())
            }
        }
    }

    render() {
        const { route } = this.props
        return (
            <div className="ThemeMain">
                <HeaderNavbar />
                {renderRoutes(route.routes, { className: "ThemeMain__page" })}
                <FooterMain />
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    user: state.User.user
})

export default connect(mapStateToProps)(ThemeMain)