import React from 'react'
import { render } from 'react-dom';
import BrowserRouter from 'react-router-dom/BrowserRouter';
import { Provider } from 'react-redux'
import configureStore from './store/ConfigureStore'
import App from './layouts/App'


//Polyfills for old browsers
require('es6-promise').polyfill()
require('isomorphic-fetch')

//Styles
import './styles/app.scss';

if (DEVELOPMENT) {
  console.log('environment:', ENVIRONMENT)
  console.log('language:', LANGUAGE)
  console.log('__INITIAL_STATE__:', window.__INITIAL_STATE__)
}

const store = configureStore(window.__INITIAL_STATE__);
render((
  <Provider store={store}>
    <BrowserRouter>
      <App />
    </BrowserRouter>
  </Provider>
), document.getElementById('app'))