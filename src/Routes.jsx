import ThemeMain from "./layouts/ThemeMain"
import NotFoundPage from './pages/NotFoundPage'
import HomePage from './pages/HomePage'
import TokenValuePage from './pages/TokenValuePage'
import RoadmapPage from './pages/RoadmapPage'
import ContactsPage from './pages/ContactsPage'
import SignInPage from './pages/SignInPage'
import SignUpPage from './pages/RegistrationPage'
import AccountPage from './pages/AccountPage'
import AgreementPage from './pages/AgreementPage'
import Url from './url'

const routes = [
    {
        component: ThemeMain,
        routes: [
            {
                path: Url.HomeMask,
                exact: true,
                component: HomePage
            },
            {
                path: Url.TokenValueMask,
                component: TokenValuePage
            },
            {
                path: Url.RoadmapMask,
                component: RoadmapPage
            },
            {
                path: Url.ContactsMask,
                component: ContactsPage
            },
            {
                path: Url.SignInMask,
                component: SignInPage
            },
            {
                path: Url.SignUpMask,
                component: SignUpPage
            },
            {
                path: Url.AccountMask,
                component: AccountPage
            },
            {
                path: Url.AgreementMask,
                component: AgreementPage
            },
            {
                path: '*',
                component: NotFoundPage
            }
        ]
    }
];

export default routes;