import API from '../api'
import Storage from '../utils/Storage'
import * as AuthUtil from '../utils/Auth'
export const USER_DEFAULT = 'USER_DEFAULT'
export const USER_AUTHENTICATED = 'USER_AUTHENTICATED'
export const USER_UNAUTHENTICATED = 'USER_UNAUTHENTICATED'
export const USER_ADD_WALLET = 'USER_ADD_WALLET'

/**
 * 
 * @param {String} url 
 */
export const Auth = ({ email, firstName, lastName, emailVerified, ethWallet, etcWallet, btcWallet, bchWallet, ltcWallet, dashWallet }) => {
  return (dispatch, getState) => {
    var v = getState().User.user
    dispatch({
      type: USER_AUTHENTICATED,
      email: email,
      firstName: firstName,
      lastName: lastName,
      emailVerified: emailVerified,
      ethWallet: ethWallet ? ethWallet : v.ethWallet,
      etcWallet: etcWallet ? etcWallet : v.etcWallet,
      btcWallet: btcWallet ? btcWallet : v.btcWallet,
      bchWallet: bchWallet ? bchWallet : v.bchWallet,
      ltcWallet: ltcWallet ? ltcWallet : v.ltcWallet,
      dashWallet: dashWallet ? dashWallet : v.dashWallet
    });
  }
}

/**
 * 
 * @param {object} param
 */
export const AddWallet = ({ ethWallet, etcWallet, btcWallet, bchWallet, ltcWallet, dashWallet }) => {
  return (dispatch, getState) => {
    dispatch({ type: USER_ADD_WALLET, ethWallet: ethWallet, etcWallet: etcWallet, btcWallet: btcWallet, bchWallet: bchWallet, ltcWallet: ltcWallet, dashWallet: dashWallet });
  }
}

/**
 * 
 * @param {String} url 
 */
export const Unauth = () => {
  return (dispatch, getState) => {
    dispatch({ type: USER_UNAUTHENTICATED });
  }
}

/**
 * 
 * @return {Bool} 
 */
export const SignOut = () => {
  const { access_token } = AuthUtil.get()
  // return API.user.revokeToken(token_type, access_token, refresh_token).then((res) => {

  //   if (res.error) {
  //     console.log('error:', res.error, res.error_description)
  //     return false
  //   }

  //   AuthHelper.remove()

  //   return true
  // }).catch((e) => console.log(e))
  AuthUtil.remove()
  return true
}


// /**
//  * Авторизация && Аутентификация пользователя
//  * @param {Object} object
//  * @param {Bool} object.allowed 
//  * 
//  * @return {Bool} 
//  */
// export const Authorize = ({ allowed }) => {

//   const { access_token, token_type, refresh_token } = AuthHelper.get()
//   const uri = window.location.pathname + window.location.search + '&allowed=' + allowed

//   return API.user.authorize(uri, token_type, access_token, refresh_token).then((res) => {
//     if (res.error) {
//       console.log('error:', res.error, res.error_description)
//       return false
//     }
//     return true
//   }).catch((e) => console.log(e))
// }

/**
 * 
 * 
 * @return {Promise<>} 
 */
export const Get = () => {
  return API.user.User()
}

/**
 * 
 * 
 * @return {Promise<>} 
 */
export const Registration = (email, password, country) => {
  return API.user.Registration(email, password, country)
}


/**
 * 
 * 
 * @return {Promise<>} 
 */
export const Authenticate = (email, password) => {
  return API.user.Authenticate(email, password)
}


/**
 * Сохранение Авторизации && Аутентификации пользователя
 * @param {Object} r 
 * 
 */
export const SaveToken = (r) => {
  AuthUtil.save(r)
}