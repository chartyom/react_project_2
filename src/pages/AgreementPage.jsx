import React, { PureComponent } from 'react'
import { HeaderNavbarIndent } from '../components/HeaderNavbar'
import { SocialLinks } from '../components/SocialLinks'
import { AgreementContent } from '../components/AgreementContent'

class AgreementPage extends PureComponent {
    componentDidMount() {
        window.scrollTo(0, 0);
    }

    render() {
        return (
            <div>
                <HeaderNavbarIndent />
                <AgreementContent />
                <SocialLinks />
            </div>
        )
    }
}


export default AgreementPage