import React, { PureComponent } from 'react'
import { Redirect } from 'react-router-dom';
import { HeaderNavbarIndent } from '../components/HeaderNavbar'
import { AccountForm } from '../components/AccountForm'
import Loader from '../components/elements/Loader'
import URL from '../url'
import * as Auth from '../utils/Auth'
import * as UserAction from '../actions/userAction'
import Store from '../utils/Storage'

class SocialsAuthPage extends PureComponent {

    constructor(props) {
        super(props);
        this.state = {
            token: "DEFAULT",
        };
    }

    componentDidMount() {
        window.scrollTo(0, 0);
    }
    handleAuth() {
        var token = Store.Cookie.getItem("token")
        if (token && token.length > 0) {
            Store.setItem("access_token", token)
            Store.Cookie.removeItem("token")
            this.setState({ token: "NOTEMPTY" })
        } else {
            this.setState({ token: "EMPTY" })
        }

    }
    render() {
        var props = this.props
        switch (props.user.token) {
            case "NOTEMPTY":
                return this.renderRedirect(URL.Account)
            case "EMPTY":
                return this.renderRedirect(URL.SignIn)
            default:
                return this.renderLocader()
        }
    }
    renderRedirect(l) {
        return (
            <Redirect to={l} />
        )
    }
    renderLocader() {
        return (
            <div className="ThemeMain__loader">
                <Loader size="big" color="orange" position="position" />
            </div>
        )
    }
}


export default SocialsAuthPage