import React, { Component } from 'react'
import { Redirect } from 'react-router-dom'
import URL from '../url'
// import { connect } from 'react-redux';
import { SignInForm } from '../components/SignInForm'
import { HeaderNavbarIndent } from '../components/HeaderNavbar'

class SignInPage extends Component {

    constructor(props) {
        super(props);
        this.state = {
            redirectToReferrer: false,
        };
        this.handleSetReferrer = this.handleSetReferrer.bind(this)
    }
    componentDidMount() {
        window.scrollTo(0, 0);
    }
    handleSetReferrer(s) {
        this.setState({ redirectToReferrer: s })
    }

    render() {
        //let { from } = this.props.location.state || { from: { pathname: '/' } }
        //if (this.props.referrerUrl) from = this.props.referrerUrl

        if (this.state.redirectToReferrer) {
            return (
                <Redirect to={URL.Account} />
            )
        }

        return (
            <div>
                <HeaderNavbarIndent />
                <SignInForm handleSetReferrer={this.handleSetReferrer} />
            </div>
        )
    }
}

// const mapStateToProps = ({ ui }) => ({
//     referrerUrl: ui.referrerUrl,
// });

export default SignInPage