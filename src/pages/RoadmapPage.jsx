import React, { PureComponent } from 'react'
import { ContentRoadmap } from '../components/ContentRoadmap'
import { HeaderNavbarIndent } from '../components/HeaderNavbar'
import { SocialLinks } from '../components/SocialLinks'
class RoadmapPage extends PureComponent {
    componentDidMount() {
        window.scrollTo(0, 0);
    }
    render() {
        return (
            <div>
                <HeaderNavbarIndent />
                <ContentRoadmap />
                <SocialLinks />
            </div>
        )
    }
}

export default RoadmapPage
