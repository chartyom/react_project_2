import React, { PureComponent } from 'react'
//import { Link } from 'react-router-dom'
//import URL from '../url'
//import ThemeMain from '../layouts/ThemeMain'
//import Storage from '../utils/Storage'
import { ContentTokenValue } from '../components/ContentTokenValue'
import { HeaderNavbarIndent } from '../components/HeaderNavbar'
import { SocialLinks } from '../components/SocialLinks'

class TokenValuePage extends PureComponent {
    componentDidMount() {
        window.scrollTo(0, 0);
    }

    render() {
        return (
            <div>
                <HeaderNavbarIndent />
                <ContentTokenValue />
                <SocialLinks />
            </div>
        )
    }
}

export default TokenValuePage