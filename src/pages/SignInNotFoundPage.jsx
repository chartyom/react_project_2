import React, { PureComponent } from 'react'

import { SignInNotFound } from '../components/SignInNotFound'

class SignInNotFoundPage extends PureComponent {
    componentDidMount() {
        window.scrollTo(0, 0);
    }
    render() {
        return (
            <div>
                <SignInNotFound />
            </div>
        )
    }
}

export default SignInNotFoundPage