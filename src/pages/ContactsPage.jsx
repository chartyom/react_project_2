import React, { PureComponent } from 'react'
//import { Link } from 'react-router-dom'
//import URL from '../url'
//import ThemeMain from '../layouts/ThemeMain'
//import Storage from '../utils/Storage'
import { ContentContacts } from '../components/ContentContacts'
import { HeaderNavbarIndent } from '../components/HeaderNavbar'

class ContactsPage extends PureComponent {
    componentDidMount() {
        window.scrollTo(0, 0);
    }
    render() {
        return (
            <div>
                <HeaderNavbarIndent />
                <ContentContacts />
            </div>
        )
    }
}

export default ContactsPage