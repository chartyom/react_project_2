import React, { PureComponent } from 'react'
import { Redirect } from 'react-router-dom';
import { HeaderNavbarIndent } from '../components/HeaderNavbar'
import { AccountForm } from '../components/AccountForm'
import Loader from '../components/elements/Loader'
import { connect } from 'react-redux';
import URL from '../url'
import * as Auth from '../utils/Auth'
import * as UserAction from '../actions/userAction'

class AccountPage extends PureComponent {
    componentDidMount() {
        window.scrollTo(0, 0);
    }
    render() {
        var props = this.props
        switch (props.user.readyState) {
            case UserAction.USER_AUTHENTICATED:
                return this.renderPage()
            case UserAction.USER_UNAUTHENTICATED:
                return this.renderRedirect()
            default:
                return this.renderLocader()
        }
    }
    renderPage() {
        return (
            <div className={this.props.className}>
                <HeaderNavbarIndent />
                <AccountForm />
            </div>
        )
    }
    renderRedirect() {
        return (
            <Redirect to={URL.SignIn} />
        )
    }
    renderLocader() {
        return (
            <div className="ThemeMain__loader">
                <Loader size="big" color="orange" position="position" />
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    user: state.User.user
})

export default connect(mapStateToProps)(AccountPage) 