import React, { Component } from 'react'
import { Link, Redirect } from 'react-router-dom'
import { RegistrationForm } from '../components/RegistrationForm'
import { HeaderNavbarIndent } from '../components/HeaderNavbar'
class RegistrationPage extends Component {

    constructor(props) {
        super(props);
        this.state = {
            redirectToReferrer: false,
        };
        this.handleSetReferrer = this.handleSetReferrer.bind(this)
    }
    componentDidMount() {
        window.scrollTo(0, 0);
    }

    handleSetReferrer(s) {
        this.setState({ redirectToReferrer: s })
    }

    render() {
        let { from } = this.props.location.state || { from: { pathname: '/' } }

        if (this.state.redirectToReferrer) {
            return (
                <Redirect to={from} />
            )
        }

        return (
            <div>
                <HeaderNavbarIndent />
                <RegistrationForm handleSetReferrer={this.handleSetReferrer} />
            </div>

        )
    }
}

export default RegistrationPage