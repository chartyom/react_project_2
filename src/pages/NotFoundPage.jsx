import React, { PureComponent } from 'react'
import { Link } from 'react-router-dom'
import { NotFound } from '../components/NotFound'

class NotFoundPage extends PureComponent {
    render() {
        return <NotFound />
    }
}

export default NotFoundPage;