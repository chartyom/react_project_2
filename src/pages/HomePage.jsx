import React, { PureComponent } from 'react'
//import { Link } from 'react-router-dom'
//import URL from '../url'
//import ThemeMain from '../layouts/ThemeMain'
//import Storage from '../utils/Storage'
import { ContentPreview } from '../components/ContentPreview'
import { ContentIntroBlock } from '../components/ContentIntroBlock'
// import { ContentСapability } from '../components/ContentСapability'
import { ContentTeam } from '../components/ContentTeam'
//import { ContentMedia } from '../components/ContentMedia'
//import { ContentInformationBlock } from '../components/ContentInformationBlock'
// import { ContentSubscribeMiniForm } from '../components/ContentSubscribeMiniForm'
import { HeaderNavbarIndent } from '../components/HeaderNavbar'
import { SocialLinks } from '../components/SocialLinks'
import { ContentDecisionBlock } from '../components/ContentDecisionBlock'
import { ContentDecisionImageBlock } from '../components/ContentDecisionImageBlock'
import { ContentMainAdvantages } from '../components/ContentMainAdvantages'
import { ContentMedia } from '../components/ContentMedia'

import { ContentBrands } from '../components/ContentBrands'

class HomePage extends PureComponent {
    componentDidMount() {
        window.scrollTo(0, 0);
    }
    render() {
        return (
            <div>
                <HeaderNavbarIndent />
                <ContentIntroBlock />
                <ContentMedia />
                <ContentDecisionBlock />
                <ContentDecisionImageBlock />
                <ContentMainAdvantages />
                <ContentPreview />
                <ContentTeam />
                <ContentBrands />
                <SocialLinks />
            </div>
        )
    }
}

export default HomePage