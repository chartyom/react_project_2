export default {
    connect: DEVELOPMENT ? "http://localhost:8040" : "https://bitkom.io",
    clientVersion: '0.1.1',
    oauth: {
        facebook: {
            clientId: DEVELOPMENT ? '130557234286607' : '131614877518662',
            redirectUrl: DEVELOPMENT ? 'http://localhost:8040/api/auth/facebook/callback' : 'http://localhost:8040/api/auth/facebook/callback',
        }
    },
};  