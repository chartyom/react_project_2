import CONFIG from '../config'
import * as UrlHelper from '../utils/UrlHelper'
import Storage from '../utils/Storage'
import { get } from '../utils/Auth'
//const URLENCODED = 'application/x-www-form-urlencoded'

export default {
    Authenticate: (email, password) => {
        return fetch(CONFIG.connect + '/api/auth/signin', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({ email: email, password: password })
        }).then(r => r.json())
    },
    User: () => {
        const { access_token } = get()
        return fetch(CONFIG.connect + '/api/user', {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': access_token
            },
        }).then(r => r.json())
    },
    AddWallet: (type, value) => {
        const { access_token } = get()
        return fetch(CONFIG.connect + '/api/user/addwallet/' + type, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': access_token
            },
            body: JSON.stringify({ wallet: value })
        }).then(r => r.json())
    },
    Registration: (email, password, country) => {
        return fetch(CONFIG.connect + '/api/auth/registration', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({ email: email, password: password, country: country })
        }).then(r => r.json())
    },
    Agreement: () => {
        return fetch(CONFIG.connect + '/api/user/agreement?lang=' + LANGUAGE, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json'
            },
        }).then(r => r.json())
    },
    AccessCountries: () => {
        return fetch(CONFIG.connect + '/api/auth/countries?lang=' + LANGUAGE, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json'
            },
        }).then(r => r.json())
    },
}
