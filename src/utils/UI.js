function GetCurrentScroll() {
    return window.scrollY || window.scollTop || document.getElementsByTagName('html')[0].scrollTop
}

module.exports = {
    GetCurrentScroll: GetCurrentScroll
}