import React from 'react'
import { Route as R, Redirect } from 'react-router-dom'
import URL from '../url'
import { IsAuthenticated } from './AuthHelper'

export const RouteAuth = ({ component, redirect, ...rest }) => {
    const auth = IsAuthenticated()
    redirect = redirect || URL.signIn
    return (
        <R {...rest} render={(props) => {
            return auth ?
                (React.createElement(component, { ...props, auth })) :
                (<Redirect to={redirect} />);
        }} />
    )
};

export const RouteUnauth = ({ component, redirect, ...rest }) => {
    const auth = IsAuthenticated()
    redirect = redirect ? redirect : URL.profile
    return (
        <R {...rest} render={(props) => {
            return auth ?
                (<Redirect to={redirect} />) :
                (React.createElement(component, { ...props, auth }));
        }} />
    )
};

export const Route = ({ component, path, ...rest }) => {
    //LANG URL
    //path = '/ru' + path
    return (
        <R {...rest} path={path} render={(props) => {
            return (React.createElement(component, { ...props }))
        }} />
    )
};