
function ls() {
    try {
        return 'localStorage' in window && window['localStorage'] !== null;
    } catch (e) {
        return false;
    }
}

function ck() {
    return navigator.cookieEnabled;
}

const Cookie = {
    getItem: function (name) {
        var matches = document.cookie.match(new RegExp(
            "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
        ));
        return matches ? decodeURIComponent(matches[1]) : undefined;
    },
    setItem: function (name, value, options) {
        options = options || {};

        var expires = options.expires;

        if (typeof expires == "number" && expires) {
            var d = new Date();
            d.setTime(d.getTime() + expires * 1000);
            expires = options.expires = d;
        }
        if (expires && expires.toUTCString) {
            options.expires = expires.toUTCString();
        }

        value = encodeURIComponent(value);

        var u = name + "=" + value;

        for (var propName in options) {
            u += "; " + propName;
            var v = options[propName];
            if (v !== true) {
                u += "=" + v;
            }
        }

        document.cookie = u;
    },
    removeItem: function (name) {
        this.setItem(name, "", {
            expires: -1
        });
    }
};


export default {
    getItem: function (key) {
        if (ls()) return localStorage.getItem(key);
        if (ck()) return Cookie.getItem(key);
        return undefined;
    },
    setItem: function (key, value, params) {
        if (ls()) return localStorage.setItem(key, value);
        if (ck()) {
            var e = params && params.expires ? params.expires : 2592000
            return Cookie.setItem(key, value, { expires: e });
        }
    },
    removeItem: function (key) {
        if (ls()) return localStorage.removeItem(key);
        if (ck()) return Cookie.removeItem(key);
    },
    Cookie
};