import Storage from "./Storage"

// Url - forms a link depending on the language
export function Url(l) {
    var p = window.location.pathname
    var languages = LANGUAGES.map(function (v) { return v.key })
    var u = p.split("/")[1];
    if (u && languages.indexOf(u) != -1) {
        return "/" + u + l
    }
    return l
}

// ChangeTo - changes the field `lang` in the cookie to the selected language
export function ChangeTo(l) {
    Storage.Cookie.setItem("lang", l, { path: "/", expires: 90000000 })
    window.location = GoToByLang(l)
}

export function GoToByLang(lang) {
    var p = window.location.pathname
    var languages = LANGUAGES.map(function (v) { return v.key })
    var s = p.split("/")
    var u = s[1];
    if (u && languages.indexOf(u) != -1) {
        s.splice(0, 2, lang)
        var r = s.join('/')
        return "/" + r
    }
    return "/" + lang + p
}