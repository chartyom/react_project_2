/**
 * Создание маркера на карте google map
 * @param {object} latlng 
 * @param {object} map 
 * @param {string} type 
 * @param {string} status 
 */
export function CustomMarker(latlng, map, type, status) {
    this.latlng_ = latlng;
    this.type_ = type || 'restaurant';
    this.status_ = status || 'close';
    this.setMap(map);
}

CustomMarker.prototype.draw = function () {
    var me = this;
    var div = this.div_;
    if (!div) {
        div = this.div_ = document.createElement('DIV');
        div.style.border = "none";
        div.style.position = "absolute";
        div.style.paddingLeft = "0px";
        div.style.cursor = 'pointer';
        //you could/should do most of the above via styling the class added below
        div.classList.add('map-marker', 'map-marker_status--' + this.status_, 'map-marker_type--' + this.type_);

        google.maps.event.addDomListener(div, "click", function (event) {
            google.maps.event.trigger(me, "click");
        });

        var panes = this.getPanes();
        panes.overlayImage.appendChild(div);
    }

    var point = this.getProjection().fromLatLngToDivPixel(this.latlng_);
    if (point) {
        div.style.left = point.x + 'px';
        div.style.top = point.y + 'px';
    }
};

CustomMarker.prototype.remove = function () {
    if (this.div_) {
        this.div_.parentNode.removeChild(this.div_);
        this.div_ = null;
    }
};