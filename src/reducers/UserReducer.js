import {
  USER_DEFAULT,
  USER_AUTHENTICATED,
  USER_UNAUTHENTICATED,
  USER_ADD_WALLET,
} from '../actions/userAction';


export default function (state = {
  user: {
    readyState: USER_DEFAULT,
    email: null,
    firstName: null,
    lastName: null,
    emailVerified: false,
    ethWallet: '',
    etcWallet: '',
    btcWallet: '',
    bchWallet: '',
    ltcWallet: '',
    dashWallet: ''
  },
}, action) {
  switch (action.type) {
    case USER_AUTHENTICATED:
      return {
        ...state,
        user: {
          readyState: USER_AUTHENTICATED,
          email: action.email,
          firstName: action.firstName,
          lastName: action.lastName,
          emailVerified: action.emailVerified,
          ethWallet: action.ethWallet,
          etcWallet: action.etcWallet,
          btcWallet: action.btcWallet,
          ltcWallet: action.ltcWallet,
          bchWallet: action.bchWallet,
          dashWallet: action.dashWallet,
        }
      };
    case USER_UNAUTHENTICATED:
      return {
        ...state,
        user: {
          ...state.user,
          readyState: USER_UNAUTHENTICATED,
        }
      };
    case USER_ADD_WALLET:
      return {
        ...state,
        user: {
          ...state.user,
          ethWallet: action.ethWallet,
          etcWallet: action.etcWallet,
          btcWallet: action.btcWallet,
          ltcWallet: action.ltcWallet,
          bchWallet: action.bchWallet,
          dashWallet: action.dashWallet,
        }
      };
    default:
      return state;
  }
}

