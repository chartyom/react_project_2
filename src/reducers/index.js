import { combineReducers } from 'redux';
import ContentPreview from './ContentPreviewReducer';
import ContentMedia from './ContentMediaReducer';
import ContentInformationBlock from './ContentInformationBlockReducer';
import ContentСapability from './ContentСapabilityReducer';
import ContentTeam from './ContentTeamReducer'
import ContentIntroBlock from './ContentIntroBlockReducer'
import ContentDecisionBlock from './ContentDecisionBlockReducer'
import ContentDecisionImageBlock from './ContentDecisionImageBlockReducer'
import ContentMainAdvantages from './ContentMainAdvantagesReducer'
import ContentTokenValue from './ContentTokenValueReducer'
import ContentContacts from './ContentContactsReducer'
import HeaderNavbar from "./HeaderNavbarReducer";
import ContentRoadmap from "./ContentRoadmapReducer"
import AccountForm from "./AccountFormReducer"
import User from "./UserReducer"


export default combineReducers({
  AccountForm,
  ContentPreview,
  ContentMedia,
  ContentInformationBlock,
  ContentСapability,
  ContentTeam,
  ContentIntroBlock,
  ContentDecisionBlock,
  ContentDecisionImageBlock,
  ContentMainAdvantages,
  ContentTokenValue,
  ContentContacts,
  HeaderNavbar,
  ContentRoadmap,
  User,
});
