import React, { PureComponent } from 'react'
import { connect } from 'react-redux';

class ContentDecisionBlock extends PureComponent {
    constructor() {
        super();
        this.state = {
            loaded: false
        };
    }

    componentDidMount() {
        var loaded = true
        setTimeout(function () {
            this.setState({ loaded }, () => {
                if (this.props.callback) this.props.callback(loaded)
            })
        }.bind(this), 400)

    }

    getData() {
        return this.props.ContentDecisionBlock;
    }
    rawMarkup() {
        var rawMarkup = this.getData().content
        return { __html: rawMarkup };
    }
    render() {
        var active = this.state.loaded ? " ContentDecisionBlock_in" : ""
        return (
            <div className={"ContentDecisionBlock" + active}>
                <div className="ContentDecisionBlock__container">
                    <h3 className="ContentDecisionBlock__container__header">
                        {this.getData().title}
                    </h3>
                    <div className="ContentDecisionBlock__content" dangerouslySetInnerHTML={this.rawMarkup()}></div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = ({ ContentDecisionBlock }) => function () {
    return {
        ContentDecisionBlock: ContentDecisionBlock
    }
};

export default connect(mapStateToProps)(ContentDecisionBlock);
