import React, { PureComponent } from 'react'
import { Link, Redirect } from 'react-router-dom'
import URL from '../../url'
// import * as userAction from '../actions/UserAction'
// import * as uiAction from '../actions/uiAction'
import { FormGroup, Field, FieldHelp, Button, Label } from '../../components/elements'
import isEmailValidator from 'validator/lib/isEmail';
import { connect } from 'react-redux';

var checkFirstnameTimer, checkLastnameTimer, checkEmailTimer

/**
 * 
 * @param {Function} handleSetReferrer
 */
class ContentSubscribeMiniForm extends PureComponent {

    constructor(props) {
        super(props);
        this.state = {
            firstnameDescription: null,
            firstnameStatus: null,
            firstnameValue: null,
            lastnameDescription: null,
            lastnameStatus: null,
            lastnameValue: null,
            emailDescription: null,
            emailStatus: null,
            emailValue: null,
            loading: false,
        };
        this.handleFirtnameValid = this.handleFirtnameValid.bind(this)
        this.handleLastnameValid = this.handleLastnameValid.bind(this)
        this.handleEmailValid = this.handleEmailValid.bind(this)
        this.handleSuccess = this.handleSuccess.bind(this)
    }

    componentWillMount() {


    }

    /*
        EMAIL
    */
    handleEmailValid(e) {
        const v = e.target.value;
        clearTimeout(checkEmailTimer)
        if (v.length > 0) {
            checkEmailTimer = setTimeout(function () {
                if (isEmailValidator(v)) {
                    if (this.state.emailStatus !== 'loading') this.setState({ emailStatus: 'loading', emailDescription: null });
                    return this.setState({
                        emailStatus: 'danger',
                        emailValue: null,
                        emailDescription: __('Введенный вами e-mail уже используется.')
                    });
                    //XHR
                    // userAction.checkEmail(v).then((res) => {
                    //     if (!res || res.error) {
                    //         throw new Error(res.error)
                    //     }
                    //     if (res.emailExist) {
                    //         this.props.dispatch(uiAction.setPhone(v))
                    //         return this.setState({ emailStatus: 'danger', emailValue: null, emailDescription: 'Введенный вами e-mail уже используется. Укажите другой e-mail или нажите на кнопку "Зарегистрироваться" для завершения регистрации' });
                    //     } else {
                    //         return this.setState({ emailStatus: 'success', emailValue: v, emailDescription: null });
                    //     }

                    // }).catch((e) => {
                    //     return this.setState({ emailStatus: 'danger', emailValue: null, emailDescription: 'На сервере произошла ошибка попробуйте зарегистрироваться позже' });
                    // })

                } else {
                    return this.setState({
                        emailStatus: 'danger',
                        emailValue: null,
                        emailDescription: __('Entered email is wrong')
                    });
                }
            }.bind(this), 500)
        } else {
            return this.setState({
                emailStatus: null,
                emailValue: null,
                emailDescription: null
            });
        }
    }


    /*
        FIRSTNAME
    */
    handleFirtnameValid(e) {
        const v = e.target.value;
        clearTimeout(checkFirstnameTimer)
        checkFirstnameTimer = setTimeout(function () {
            if (v.length > 1)
                return this.setState({
                    firstnameStatus: 'success',
                    firstnameValue: v,
                    firstnameDescription: null
                });
            return this.setState({
                firstnameStatus: 'danger',
                firstnameValue: null,
                firstnameDescription: __('Entered firstname is short')
            });
        }.bind(this), 500)
    }

    /*
        LASTNAME
    */
    handleLastnameValid(e) {
        const v = e.target.value;
        clearTimeout(checkLastnameTimer)
        checkLastnameTimer = setTimeout(function () {
            if (v.length > 1)
                return this.setState({ lastnameStatus: 'success', lastnameValue: v, lastnameDescription: null });
            return this.setState({ lastnameStatus: 'danger', lastnameValue: null, lastnameDescription: __('Entered secondname is short') });
        }.bind(this), 500)
    }

    handleSuccess(e) {

        let state = {}
        // if (
        //     this.state.firstnameStatus !== 'success'
        // ) {
        //     state.firstnameStatus = 'danger'
        //     state.firstnameDescription = __('Enter your firstname')
        // }
        if (
            this.state.emailStatus !== 'success'
        ) {
            state.emailStatus = 'danger'
            state.emailDescription = __('Enter your e-mail')
        }
        // if (
        //     this.state.lastnameStatus !== 'success'
        // ) {
        //     state.lastnameStatus = 'danger'
        //     state.lastnameDescription = __('Enter your secondname')
        // }

        if (
            this.state.emailValue
        ) {

            this.setState({ loading: true })
            state.loading = false
            // return userAction.registrationSendSMS({
            //     phone: this.state.phoneValue,
            //     firstName: this.state.firstnameValue,
            //     lastName: this.state.lastnameValue,
            //     email: this.state.emailValue,
            //     code: this.state.referrerCodeValue
            // }).then((res) => {
            //     if (!res || res.error) {
            //         throw new Error(res.error)
            //     }
            //     if (res.key) {
            //         state.key = res.key
            //         state.loading = false
            //         return this.setState(state);
            //     }
            // }).catch((e) => {
            //     this.setState({
            //         firstnameStatus: 'danger',
            //         firstnameDescription: __('There was an error sending the message'),
            //         loading: false
            //     })
            //     return console.log(e);
            // })
        }

        this.setState(state)

    }
    render() {
        var buttonDisabled = false
        if (this.state.emailStatus === 'loading' || this.state.loading) buttonDisabled = true
        return (
            <div className="ContentSubscribeMiniForm">
                <div className="ContentSubscribeMiniForm__container">
                    <h3 className="ContentSubscribeMiniForm__container__header">{__('SUBSCRIBE')}</h3>
                    <div className="ContentSubscribeMiniForm__content subscribe">
                        <FormGroup status={this.state.emailStatus}>
                            <Label require >{__('E-mail')}</Label>
                            <Field placeholder={__("Enter your e-mail")} onChange={this.handleEmailValid} defaultValue={this.state.emailValue} />
                            <FieldHelp>{this.state.emailDescription}</FieldHelp>
                        </FormGroup>

                        {/* <FormGroup status={this.state.firstnameStatus} >

                            <Label require >{__("Firstname")}</Label>
                            <Field placeholder={__("Enter your firstname")} onChange={this.handleFirtnameValid} defaultValue={this.state.firstnameValue} />
                            <FieldHelp>{this.state.firstnameDescription}</FieldHelp>
                        </FormGroup> */}
                        {/* <FormGroup status={this.state.lastnameStatus}>
                            <Label require >Lastname</Label>
                            <Field placeholder="Enter your lastname" onChange={this.handleLastnameValid} defaultValue={this.state.lastnameValue} />
                            <FieldHelp>{this.state.lastnameDescription}</FieldHelp>
                        </FormGroup> */}

                        <button className={"subscribe__button" + (this.state.loading ? ' loading' : '')} onClick={this.handleSuccess} disabled={buttonDisabled}>{__("Subscribe")}</button>
                    </div>
                </div>
            </div>
        )
    }
}

export default connect()(ContentSubscribeMiniForm)