import React, { PureComponent } from 'react'
import { Link } from 'react-router-dom'
import { GetCurrentScroll } from '../../utils/UI'
import API from '../../api/User'
import Loader from '../elements/Loader'

class AgreementContent extends PureComponent {

    constructor(props) {
        super(props);
        this.state = {
            readyState: "DEFAULT",
            title: null,
            content: null,
        };
    }

    componentDidMount() {
        this.handleUpload()
    }

    handleUpload() {
        API.Agreement().then((res) => {
            if (!res || res.error) {
                return console.log(res.error);
            }
            if (!res.agreement) {
                return console.log("Error");
                this.setState({ readyState: "FAILED", title: "Error", content: "Error loading" })
            }
            this.setState({ readyState: "LOADED", title: res.agreement.title, content: res.agreement.content })
        }).catch((e) => {
            return console.log(e);
        })
    }

    rawMarkup() {
        return { __html: this.state.content };
    }
    render() {
        if (this.state.readyState == "DEFAULT") {
            return (
                <div className="AgreementContent">
                    <div className="AgreementContent__container">
                        <h3 className="AgreementContent__container__header">
                            <Loader size="big" color="orange" position="position" />
                        </h3>
                    </div>
                </div>

            )
        } else {
            return (
                <div className="AgreementContent">
                    <div className="AgreementContent__container">
                        <h3 className="AgreementContent__container__header">
                            {this.state.title}
                        </h3>
                        <div className=" AgreementContent__content" dangerouslySetInnerHTML={this.rawMarkup()}></div>
                    </div>
                </div>

            )
        }

    }
}


export default AgreementContent;
