import React, { PureComponent } from 'react'
import { connect } from 'react-redux';

class ContentСapability extends PureComponent {
    getData() {
        return this.props.ContentСapability;
    }

    handleList() {
        return this.getData().list.map(function (v, i) {
            if (i % 2 == 0) {
                return (
                    <div key={i} className="ContentСapability__list__item">
                        <div className="ContentСapability__content content">
                            <h3 className="content__header">{v.title}</h3>
                            <div className="content__body">{v.content}</div>
                        </div>
                        <div className="ContentСapability__separator"></div>
                        <div className="ContentСapability__image">
                            <div className="ContentСapability__image__frame"></div>
                        </div>
                    </div>
                )
            }
            return (
                <div key={i} className="ContentСapability__list__item">
                    <div className="ContentСapability__image">
                        <div className="ContentСapability__image__frame"></div>
                    </div>
                    <div className="ContentСapability__separator"></div>
                    <div className="ContentСapability__content content">
                        <h3 className="content__header">{v.title}</h3>
                        <div className="content__body">{v.content}</div>
                    </div>
                </div>
            )


        })
    }

    render() {
        return (
            <div className="ContentСapability">
                <div className="ContentСapability__container">
                    <h3 className="ContentСapability__container__header">
                        {this.getData().title}
                    </h3>
                    <div className="ContentСapability__list">
                        {this.handleList()}
                    </div>
                </div>
            </div>

        )
    }
}


const mapStateToProps = ({ ContentСapability }) => function () {
    return {
        ContentСapability: ContentСapability
    }
};

export default connect(mapStateToProps)(ContentСapability);

