import React, { PureComponent } from 'react'
import { Link, Route } from 'react-router-dom'
import URL from '../url'
import { isAuthenticated } from '../utils/AuthHelper'
import { getQueryParameterByName } from '../utils/UrlHelper'
import { Button } from '../components/elements'
import Storage from '../utils/Storage'
import { Loader } from './elements'
import API from '../api'

/**
 * 
 * @param {String} referrerUrl
 */
class AccessPermissionPanel extends PureComponent {

    constructor(props) {
        super(props);
        this.state = {
            responseType: null,
            uriAllow: null,
            uriDisallow: null,
            accessToken: null,
            loading: true,
            rules: null,
            appName: null
        };
    }

    componentDidMount() {
        this.handleAuthenticate()
        this.handleLoad()
    }

    handleAuthenticate() {
        if (isAuthenticated()) {
            const uriAllow = this.props.referrerUrl + '&allowed=true'
            const uriDisallow = this.props.referrerUrl + '&allowed=false'
            const accessToken = Storage.getItem('access_token')
            const responseType = getQueryParameterByName('response_type')
            this.setState({ responseType: responseType, uriAllow: uriAllow, uriDisallow: uriDisallow, accessToken: accessToken })
        } else {
            const responseType = getQueryParameterByName('response_type')
            this.setState({ responseType: responseType })
        }
    }

    handleLoad() {
        const clientId = getQueryParameterByName('client_id')
        const scope = getQueryParameterByName('scope')
        if (clientId && scope) {
            Promise.all([
                API.rules.getList(scope),
                API.client.getName(clientId)
            ]).then(function (values) {
                var rules = values[0] && values[0].rules ? values[0].rules : []
                var appName = values[1] && values[1].name ? values[1].name : ''

                this.setState({ rules, appName, loading: false })
            }.bind(this))
        }
    }

    render() {
        if (this.state.loading) {
            return (
                <div className="AccessPermissionPanel">
                    <Loader color={'orange'} position={'center'} />
                </div>
            )
        } else {
            return (
                <div className="AccessPermissionPanel">
                    <div className="AccessPermissionPanel__title">Приложение <b>{this.state.appName}</b> запрашивает доступ к вашим данным на Name:</div>
                    <AccessPermissionListItem list={this.state.rules} />
                    <div className="AccessPermissionPanel__footer">
                        <form method="POST" action={this.state.uriAllow}>
                            <input type="text" name="access_token" defaultValue={this.state.accessToken} hidden />
                            <Button type="submit" className="warning">Разрешить</Button>
                        </form>
                        <form method="POST" action={this.state.uriDisallow}>
                            <input type="text" name="access_token" defaultValue={this.state.accessToken} hidden />
                            <Button type="submit" className="round">Запретить</Button>
                        </form>
                    </div>
                </div>
            )
        }
    }
}

export default AccessPermissionPanel


/**
 * 
 * @param {Array} list
 */
class AccessPermissionListItem extends PureComponent {
    render() {
        return (
            <ul className="AccessPermissionPanel__content">
                {
                    this.props.list.map((item, i) => (
                        <li key={i} className="AccessPermissionPanel__content__item">{item}</li>
                    ))
                }
            </ul>
        )
    }
}
