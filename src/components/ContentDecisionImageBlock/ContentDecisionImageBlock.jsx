import React, { PureComponent } from 'react'
import { connect } from 'react-redux';
import { GetCurrentScroll } from '../../utils/UI'

import img_main from '../../images/decision.png'

class ContentDecisionImageBlock extends PureComponent {

    constructor() {
        super();
        this.state = {
            loaded: false
        };
        this.handleScroll = this.handleScroll.bind(this)
    }

    componentDidMount() {
        document.addEventListener('scroll', this.handleScroll, true);
    }

    componentWillUnmount() {
        document.removeEventListener('scroll', this.handleScroll, true);
    }

    handleScroll() {
        const currentScroll = GetCurrentScroll()
        const offsetTop = this.block.offsetTop
        const _innerHeight = innerHeight / 3
        const loaded = currentScroll + window.innerHeight - _innerHeight >= offsetTop &&
            currentScroll < offsetTop + this.block.offsetHeight - _innerHeight
        if (this.state.loaded == false) {
            this.setState({ loaded }, () => {
                if (this.props.callback) this.props.callback(loaded)
            })
        }
    }


    getData() {
        return this.props.ContentDecisionImageBlock;
    }
    render() {
        var active = this.state.loaded ? " ContentDecisionImageBlock_in" : ""
        return (
            <div className={"ContentDecisionImageBlock" + active} ref={(div) => {
                this.block = div;
            }}>
                <div className="ContentDecisionImageBlock__container">
                    <h3 className="ContentDecisionImageBlock__container__header">
                        {this.getData().title}
                    </h3>
                    <div className=" ContentDecisionImageBlock__content">
                        <a href={img_main} target="_blank" title="Open in new tab">
                            <img src={img_main} alt={this.getData().title} />
                        </a>
                    </div>
                </div>
            </div>

        )
    }
}

const mapStateToProps = ({ ContentDecisionImageBlock }) => function () {
    return {
        ContentDecisionImageBlock: ContentDecisionImageBlock
    }
};

export default connect(mapStateToProps)(ContentDecisionImageBlock);
