import React, { PureComponent } from 'react'
import { Link } from 'react-router-dom'
import URL from "../../url"
class NotFound extends PureComponent {
    render() {
        return (
            <div className="NotFound">
                <div className="NotFound__container">
                    <h1 className="NotFound__container__header">
                        {__("Page not found")}
                    </h1>
                    <div className="NotFound__content">
                        <p>{__("This page has been deleted or never created")}</p>
                        <Link to={URL.Home} className="button primary">{__("Go to main")}</Link>
                    </div>
                </div>
            </div>

        )
    }
}

export default NotFound
