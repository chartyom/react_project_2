import React, { PureComponent } from 'react'
import { Link } from 'react-router-dom'
import URL from '../../url'
import * as userAction from '../../actions/userAction'
//import * as uiAction from '../actions/uiAction'
import { FormGroup, Field, FieldHelp, Button, Label, Checkbox, Select } from '../elements'
import isEmailValidator from 'validator/lib/isEmail';
import matchesValidator from 'validator/lib/matches'
import { connect } from 'react-redux';
import API from '../../api/User'
/**
 * 
 * @param {Function} handleSetReferrer
 */
class RegistrationForm extends PureComponent {

    constructor(props) {
        super(props);
        this.state = {
            passwordDescription: null,
            passwordStatus: null,
            passwordValue: null,
            passwordAgainDescription: null,
            passwordAgainStatus: null,
            passwordAgainValue: null,
            emailDescription: null,
            emailStatus: null,
            emailValue: null,
            checkStatus: null,
            checkValue: false,
            loading: false,
            success: false,
            countriesReadyState: "DEFAULT",
            countries: [],
            countryValue: null,
            countryStatus: null,
            countryDescription: null,
        };
        this.handleSuccess = this.handleSuccess.bind(this)
        this.handleEmailValid = this.handleEmailValid.bind(this)
        this.handleCheckboxValid = this.handleCheckboxValid.bind(this)
        this.handlePasswordValid = this.handlePasswordValid.bind(this)
        this.handlePasswordAgainValid = this.handlePasswordAgainValid.bind(this)
        this.handleSelectValid = this.handleSelectValid.bind(this)

    }

    componentDidMount() {
        this.handleUpload()
    }

    handleUpload() {
        API.AccessCountries().then((res) => {
            if (!res || res.error) {
                return console.log(res.error);
            }
            if (!res.countries) {
                return console.log("Error");
                this.setState({ countriesReadyState: "FAILED", countryDescription: "Error loading countries, please update page", countryStatus: 'danger' })
            }
            this.setState({ countriesReadyState: "LOADED", countryDescription: __('List of countries eligible to participate in Bitkom ICO'), countries: res.countries, countryStatus: null })
        }).catch((e) => {
            return console.log(e);
        })
    }

    /*
        EMAIL
    */
    handleEmailValid(e) {
        const v = e.target.value;
        if (v.length > 0) {
            if (isEmailValidator(v)) {
                return this.setState({ emailStatus: 'success', emailValue: v, emailDescription: null });
            } else {
                return this.setState({ emailStatus: 'danger', emailValue: v, emailDescription: __('Invalid e-mail') });
            }
        } else {
            return this.setState({ emailStatus: null, emailValue: null, emailDescription: null });
        }
    }

    /*
    CHECKBOX
*/
    handleCheckboxValid(e) {
        const v = e.target.checked;
        var s = v ? 'success' : null
        this.setState({ checkValue: v, checkStatus: s })
    }

    /*
    SELECT
    */
    handleSelectValid(e) {
        const v = e.target.value;
        var s = v ? 'success' : null
        this.setState({ countryValue: v, countryStatus: s, countryDescription: __('List of countries eligible to participate in Bitkom ICO') })
    }

    handlePasswordValid(e) {
        const v = e.target.value;
        if (v.length > 0) {
            if (v.length > 5) {
                return this.setState({ passwordStatus: 'success', passwordValue: v, passwordDescription: null });
            }
            return this.setState({ passwordStatus: 'danger', passwordValue: v, passwordDescription: __('Insecure password') });
        }
        return this.setState({ passwordStatus: null, passwordValue: null, passwordDescription: null });
    }

    handlePasswordAgainValid(e) {
        const v = e.target.value;

        if (this.state.passwordValue == v && v.length > 0) {
            return this.setState({ passwordAgainStatus: 'success', passwordAgainValue: v, passwordAgainDescription: null });
        }
        return this.setState({ passwordAgainStatus: null, passwordAgainValue: null, passwordAgainDescription: null });
    }

    /*
        SUCCESS
    */
    handleSuccess(e) {
        let state = {}


        if (
            !this.state.checkValue
        ) {
            state.checkStatus = 'danger'
        }

        if (
            !this.state.emailValue || this.state.emailValue.length == 0
        ) {
            state.emailStatus = 'danger'
            state.emailDescription = __('Enter your e-mail')
        }

        if (
            !this.state.passwordValue || this.state.passwordValue.length == 0
        ) {
            state.passwordStatus = 'danger'
            state.passwordDescription = __('Enter password')
        }

        if (
            !this.state.countryValue || this.state.countryValue.length == 0
        ) {
            state.countryStatus = 'danger'
        }

        if (
            (!this.state.passwordAgainValue || this.state.passwordAgainValue.length == 0) || this.state.passwordAgainValue != this.state.passwordValue
        ) {
            state.passwordAgainStatus = 'danger'
            state.passwordAgainDescription = __('Passwords do not match')
        }

        if (
            this.state.passwordStatus == 'success' &&
            this.state.emailStatus == 'success' &&
            this.state.passwordAgainStatus == 'success' &&
            this.state.countryStatus == 'success' &&
            this.state.checkStatus == 'success'
        ) {
            this.setState({ loading: true })
            return userAction.Registration(
                this.state.emailValue,
                this.state.passwordValue,
                this.state.countryValue
            ).then((res) => {
                if (!res || res.error) {
                    //throw new Error(res.error)
                    this.setState({ emailStatus: 'danger', emailDescription: __('There was an error sending the message'), loading: false })
                    return console.log(res.error);
                }
                if (!res.ok) {
                    this.setState({ emailStatus: 'danger', emailDescription: __('There was an error sending the message'), loading: false })
                }

                return this.setState({ success: true })

            }).catch((e) => {
                this.setState({ emailStatus: 'danger', emailDescription: __('There was an error sending the message'), loading: false })
                return console.log(e);
            })
        }

        this.setState(state)

    }


    render() {
        if (this.state.success) {
            return (
                <div className="RegistrationForm_wrapper">
                    <div className="RegistrationForm">
                        {this.renderSuccess()}
                    </div>
                </div>
            )
        } else {
            return (
                <div className="RegistrationForm_wrapper">
                    <div className="RegistrationForm">
                        <h1 className="RegistrationForm__title">{__("Sign Up")}</h1>
                        {this.renderForm()}
                    </div>
                </div>
            )
        }

    }

    renderForm() {
        var buttonDisabled = false
        if (this.state.loading) buttonDisabled = true
        return (

            <div className="RegistrationForm__main">
                <FormGroup status={this.state.emailStatus} >
                    <Label require >{__("E-mail")}</Label>
                    <Field placeholder={__("Enter e-mail")} type="email" onChange={this.handleEmailValid} defaultValue={this.state.emailValue} />
                    <FieldHelp>{this.state.emailDescription}</FieldHelp>
                </FormGroup>
                <FormGroup status={this.state.passwordStatus} >
                    <Label require >{__("Password")}</Label>
                    <Field placeholder={__("Enter password")} type="password" onChange={this.handlePasswordValid} defaultValue={this.state.passwordValue} />
                    <FieldHelp>{this.state.passwordDescription}</FieldHelp>
                </FormGroup>
                <FormGroup status={this.state.passwordAgainStatus} >
                    <Label require >{__("Password again")}</Label>
                    <Field placeholder={__("Enter the password again")} type="password" onChange={this.handlePasswordAgainValid} defaultValue={this.state.passwordAgainValue} />
                    <FieldHelp>{this.state.passwordAgainDescription}</FieldHelp>
                </FormGroup>
                <FormGroup status={this.state.countryStatus} >
                    <Label require >{__("Select countries")}</Label>
                    <Select list={this.state.countries} onChange={this.handleSelectValid} />
                    <FieldHelp>{this.state.countryDescription}</FieldHelp>
                </FormGroup>
                <FormGroup status={this.state.checkStatus} >
                    <Checkbox onChange={this.handleCheckboxValid}>
                        I certify that I am 18 years of age or older, and I agree to the <a href={this.props.TokenSaleTerms} target="_blank" >{__("Token sale terms")}</a>
                    </Checkbox>
                </FormGroup>
                <Button className={"warning block" + (this.state.loading ? ' loading' : '')} onClick={this.handleSuccess} disabled={buttonDisabled}>{__('Sign Up')}</Button>
            </div>

        )
    }
    renderSuccess() {
        return (
            <div className="RegistrationForm__main">
                <h2 className="RegistrationForm__main__header">{__('Success')}</h2>
                <div className="RegistrationForm__main__message">
                    {__('Check your email and follow the instructions')}
                </div>
                <Link to={URL.SignIn} className="button primary">{__("Sign in")}</Link>
            </div>
        )
    }
}


const mapStateToProps = ({ HeaderNavbar }) => function () {
    return {
        TokenSaleTerms: HeaderNavbar.TokenSaleTerms
    }
};

export default connect(mapStateToProps)(RegistrationForm);