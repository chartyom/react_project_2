import React, { PureComponent } from 'react'
import { connect } from 'react-redux';

class ContentInformationBlock extends PureComponent {
    getData() {
        return this.props.ContentInformationBlock;
    }
    render() {
        return (
            <div className="ContentInformationBlock">
                <div className="ContentInformationBlock__container">
                    <h3 className="ContentInformationBlock__container__header">
                        {this.getData().title}
                    </h3>
                    <div className="ContentInformationBlock__content">
                        {this.getData().content}
                    </div>
                </div>
            </div>

        )
    }
}

const mapStateToProps = ({ ContentInformationBlock }) => function () {
    return {
        ContentInformationBlock: ContentInformationBlock
    }
};

export default connect(mapStateToProps)(ContentInformationBlock);
