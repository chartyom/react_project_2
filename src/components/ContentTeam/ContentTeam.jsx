import React, { PureComponent } from 'react'
import { Link } from 'react-router-dom'
import { connect } from 'react-redux';
import { GetCurrentScroll } from '../../utils/UI'

class ContentTeam extends PureComponent {
    constructor() {
        super();
        this.state = {
            loaded: false
        };
        this.handleScroll = this.handleScroll.bind(this)
    }

    componentDidMount() {
        document.addEventListener('scroll', this.handleScroll, true);
    }

    componentWillUnmount() {
        document.removeEventListener('scroll', this.handleScroll, true);
    }

    handleScroll() {
        const currentScroll = GetCurrentScroll()
        const offsetTop = this.block.offsetTop
        const _innerHeight = innerHeight / 3
        const loaded = currentScroll + window.innerHeight - _innerHeight >= offsetTop &&
            currentScroll < offsetTop + this.block.offsetHeight - _innerHeight
        if (this.state.loaded == false) {
            this.setState({ loaded }, () => {
                if (this.props.callback) this.props.callback(loaded)
            })
        }
    }

    getData() {
        return this.props.ContentTeam;
    }

    handleList() {
        return this.getData().list.map(function (v, i) {
            var photo = v.photo.length > 0 ? <img src={v.photo} alt={v.name} /> : ""
            var linkedin = v.linkedin && v.linkedin.length > 0 ? <a href={v.linkedin} title="Show in linkedin" className="content__socials__link content__socials__link--linkedin" target="_blank"></a> : ""
            var fb = v.facebook && v.facebook.length > 0 ? <a href={v.facebook} title="Show in facebook" className="content__socials__link content__socials__link--facebook" target="_blank"></a> : ""
            var twitter = v.twitter && v.twitter.length > 0 ? <a href={v.twitter} title="Show in twitter" className="content__socials__link content__socials__link--twitter" target="_blank"></a> : ""
            return (
                <div key={i} className="ContentTeam__list__item content">
                    <div className="content__frame">
                        <div className="content__frame__image">{photo}</div>
                    </div>
                    <h4 className="content__name">{v.name}</h4>
                    <div className="content__info">{v.info}</div>
                    <div className="content__socials">
                        {linkedin}
                        {fb}
                        {twitter}
                    </div>
                </div>
            )
        })
    }

    render() {
        var active = this.state.loaded ? " ContentTeam_in" : ""
        return (
            <div className={"ContentTeam" + active} ref={(div) => {
                this.block = div;
            }}>
                <div className="ContentTeam__container">
                    <h3 className="ContentTeam__container__header">{this.getData().title}</h3>
                    <div className="ContentTeam__list">
                        {this.handleList()}
                    </div>
                </div>
            </div>

        )
    }
}



const mapStateToProps = ({ ContentTeam }) => function () {
    return {
        ContentTeam: ContentTeam
    }
};

export default connect(mapStateToProps)(ContentTeam);
