import React, { PureComponent } from 'react'
import { IframeAutosize } from '../helpers'
import { connect } from 'react-redux';

class ContentMedia extends PureComponent {
    getData() {
        return this.props.ContentMedia;
    }
    render() {
        return (
            <div className="ContentMedia">
                <div className="ContentMedia__container">
                    <h3 className="ContentMedia__container__header">
                        {this.getData().title}
                    </h3>
                    <div className="ContentMedia__player">
                        <IframeAutosize className="ContentMedia__player__iframe" src={this.getData().link} />
                    </div>
                </div>
            </div>

        )
    }
}


const mapStateToProps = ({ ContentMedia }) => function () {
    return {
        ContentMedia: ContentMedia
    }
};

export default connect(mapStateToProps)(ContentMedia);