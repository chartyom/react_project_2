import React, { PureComponent } from 'react'
import { Link } from 'react-router-dom'
import URL from "../../url"
class SignInNotFound extends PureComponent {
    render() {
        return (
            <div className="SignInNotFound">
                <div className="SignInNotFound__container">
                    <h1 className="SignInNotFound__container__header">
                        {__("Page temporarily unavailable")}
                    </h1>
                    <div className="SignInNotFound__content">
                        <p>{__("Preparation for start is in progress")}</p>
                        <Link to={URL.Home} className="button primary">{__("Go to main")}</Link>
                    </div>
                </div>
            </div>

        )
    }
}

export default SignInNotFound
