import React, { Component } from 'react'
import { NavLink, Link } from 'react-router-dom'

import URL from '../../url'
import HeaderNavbarButtons from './HeaderNavbarButtons'
import img_logo from '../../images/logo.png'
import HeaderNavbarAccount from './HeaderNavbarAccount'

var CONTENT = [
    {
        title: __("HOME"),
        link: URL.Home,
    },
    {
        title: __("ICO"),
        link: URL.TokenValue,
    },
    {
        title: __("ROADMAP"),
        link: URL.Roadmap,
    },
    {
        title: __("CONTACTS"),
        link: URL.Contacts,
    },
]

class HeaderNavbar extends Component {

    handleMenu() {
        return CONTENT.map(function (v, i) {
            return (
                <li key={i} className="menu-list__item">
                    <NavLink to={v.link} activeClassName="menu-list__item__link--active" exact title={v.title} className="menu-list__item__link" >{v.title}</NavLink >
                </li>
            )
        })
    }
    render() {
        return (
            <nav className="HeaderNavbar HeaderNavbar--top-fixed" id="nav">
                <div className="HeaderNavbar__container">
                    <div className="HeaderNavbar__logo">
                        <Link to={URL.Home} className="HeaderNavbar__logo__link">
                            <img src={img_logo} alt={__("Bitkom")} className="HeaderNavbar__logo__image" />
                        </Link>
                    </div>
                    <HeaderNavbarButtons />
                    <div className="HeaderNavbar__menu">
                        <ul className="HeaderNavbar__menu__list menu-list">
                            {this.handleMenu()}
                            <HeaderNavbarAccount />
                        </ul>
                    </div>
                </div>
            </nav>
        )
    }
}


export default HeaderNavbar;