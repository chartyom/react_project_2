import React, { Component } from 'react'
import { connect } from 'react-redux';

class HeaderNavbarButtons extends Component {
    getData() {
        return this.props.HeaderNavbar;
    }
    render() {
        return (
            <div className="HeaderNavbar__buttons">
                <div className="HeaderNavbar__whitepaper">
                    <a href={this.getData().Onepager} className="HeaderNavbar__whitepaper__link" target="_blank">{__("Onepager")}</a>
                </div>
                <div className="HeaderNavbar__whitepaper">
                    <a href={this.getData().Whitepaper} className="HeaderNavbar__whitepaper__link" target="_blank">{__("Whitepaper")}</a>
                </div>
            </div>
        )
    }
}


const mapStateToProps = ({ HeaderNavbar }) => function () {
    return {
        HeaderNavbar: HeaderNavbar
    }
};

export default connect(mapStateToProps)(HeaderNavbarButtons);