import React, { Component } from 'react'
import { connect } from 'react-redux';
import { USER_AUTHENTICATED } from '../../actions/userAction';
import { Link } from 'react-router-dom'
import URL from '../../url'
class HeaderNavbarAccount extends Component {
    render() {
        if (this.props.user && this.props.user.readyState == USER_AUTHENTICATED) {
            var name = this.props.user.email
            name = name.split('@')[0]
            return (
                <li className="menu-list__item menu-list__item--pc">
                    <L t={URL.Account} n={name} />
                </li>
            )
        }
        return (
            <li className="menu-list__item menu-list__item--pc">
                <L t={URL.SignIn} n={__("SIGN IN")} />
            </li>
        )
    }
}

const L = ({ t, n }) => <Link to={t} className="menu-list__item__link" >{n}</Link>

const mapStateToProps = ({ User }) => ({
    user: User.user
});

export default connect(mapStateToProps)(HeaderNavbarAccount);