import React, { PureComponent } from 'react'
import { Link, Redirect } from 'react-router-dom'
import URL from '../../url'
// import * as userAction from '../../actions/UserAction'
// import * as uiAction from '../actions/uiAction'
import { FormGroup, Field, FieldHelp, Button, Label, Checkbox } from '../elements'
import matchesValidator from 'validator/lib/matches'
import isEmailValidator from 'validator/lib/isEmail';
import { connect } from 'react-redux';
import { Auth, Get, Authenticate, SaveToken } from '../../actions/userAction'
import CONFIG from '../../config'
var FB_CLIENT_ID = CONFIG.oauth.facebook.clientId
var FB_REDIRECT_URL = CONFIG.oauth.facebook.redirectUrl
/**
 * 
 * @param {Function} handleSetReferrer
 */
class SignInForm extends PureComponent {

    constructor(props) {
        super(props);
        this.state = {
            passwordDescription: null,
            passwordStatus: null,
            passwordValue: null,
            emailDescription: null,
            emailStatus: null,
            emailValue: null,
            isRedirect: false,
            // checkStatus: null,
            // checkValue: false,
            loading: false,
        };
        this.handleSuccess = this.handleSuccess.bind(this)
        this.handleEmailValid = this.handleEmailValid.bind(this)
        //this.handleCheckboxValid = this.handleCheckboxValid.bind(this)
        this.handlePasswordValid = this.handlePasswordValid.bind(this)
    }

    /*
        EMAIL
    */
    handleEmailValid(e) {
        const v = e.target.value;
        if (v.length > 0) {
            if (isEmailValidator(v)) {
                return this.setState({ emailStatus: 'success', emailValue: v, emailDescription: null });
            } else {
                return this.setState({ emailStatus: 'danger', emailValue: v, emailDescription: __('Invalid e-mail') });
            }
        } else {
            return this.setState({ emailStatus: null, emailValue: null, emailDescription: null });
        }
    }

    handlePasswordValid(e) {
        const v = e.target.value;
        if (v.length > 0) {
            if (v.length > 5) {
                return this.setState({ passwordStatus: 'success', passwordValue: v, passwordDescription: null });
            }
            return this.setState({ passwordStatus: 'danger', passwordValue: v, passwordDescription: __('Insecure password') });
        }
        return this.setState({ passwordStatus: null, passwordValue: null, passwordDescription: null });
    }

    /*
        CHECKBOX
    */
    // handleCheckboxValid(e) {
    //     const v = e.target.checked;
    //     var s = v ? 'success' : null
    //     this.setState({ checkValue: v, checkStatus: s })
    // }

    /*
        SUCCESS
    */
    handleSuccess(e) {

        let state = {}
        if (!isEmailValidator(this.state.emailValue)) {
            state.emailStatus = 'danger'
            state.emailDescription = __('Enter your e-mail')
        }

        if (
            !this.state.passwordValue || this.state.passwordValue.length == 0
        ) {
            state.passwordStatus = 'danger'
            state.passwordDescription = __('Enter password')
        }

        if (
            this.state.passwordStatus == 'success' &&
            isEmailValidator(this.state.emailValue)
        ) {
            if (DEVELOPMENT) console.log("passwordStatus == 'success' && isEmailValidator(this.state.emailValue)")
            this.setState({ loading: true })
            return Authenticate(
                this.state.emailValue,
                this.state.passwordValue
            ).then((res) => {
                if (!res || res.error) {
                    //throw new Error(res.error)
                    this.setState({ emailStatus: 'danger', emailDescription: __('There was an error sending the message'), loading: false })
                    return console.log(res.error);
                }
                if (!res.ok) {
                    return this.setState({ emailStatus: 'danger', emailDescription: __('The combination of login and password is not entered correctly or your account is not verified'), loading: false })
                }
                SaveToken({ access_token: res.token })

                Get().then((res) => {
                    if (!res || res.error) {
                        return console.log(res.error);
                    }
                    if (!res.ok) {
                        return console.log("Error");
                    }
                    this.props.dispatch(Auth(res.user));
                }).catch((e) => {
                    return console.log(e);
                })

                return setTimeout(() => (this.setState({ isRedirect: true })), 100)

            }).catch((e) => {
                this.setState({ emailStatus: 'danger', emailDescription: __('There was an error sending the message'), loading: false })
                return console.log(e);
            })
        }

        this.setState(state)

    }


    render() {
        if (this.state.isRedirect) {
            return (
                <Redirect to={URL.Account} />
            )
        }
        return (
            <div className="signInForm_wrapper">
                <div className="signInForm">
                    <div className="signInForm__avatar">
                        <div className="signInForm__avatar__user"></div>
                    </div>
                    {this.renderForm()}
                </div>
            </div>
        )
    }

    renderForm() {
        var buttonDisabled = false
        if (this.state.loading) buttonDisabled = true
        // var fbAuthLink = "https://www.facebook.com/v2.11/dialog/oauth?response_type=code&client_id=" + FB_CLIENT_ID + "&redirect_uri=" + FB_REDIRECT_URL + "&scope=public_profile,email"
        // var fbbtn = this.state.checkValue ? <a href={fbAuthLink} target="_blank" className="signInForm__socials__item signInForm__socials__item_facebook">{__("Sign in with Facebook")}
        // </a> : <button target="_blank" disabled="true" className="signInForm__socials__item signInForm__socials__item_facebook signInForm__socials__item_disabled">{__("Sign in with Facebook")}
        //     </button>
        return (
            <div className="signInForm__main">
                <FormGroup status={this.state.emailStatus} >
                    <Label require >{__("E-mail")}</Label>
                    <Field placeholder={__("Enter e-mail")} type="email" onChange={this.handleEmailValid} defaultValue={this.state.emailValue} />
                    <FieldHelp>{this.state.emailDescription}</FieldHelp>
                </FormGroup>
                <FormGroup status={this.state.passwordStatus} >
                    <Label require >{__("Password")}</Label>
                    <Field placeholder={__("Enter password")} type="password" onChange={this.handlePasswordValid} defaultValue={this.state.passwordValue} />
                    <FieldHelp>{this.state.passwordDescription}</FieldHelp>
                </FormGroup>

                <Button className="warning block" onClick={this.handleSuccess} disabled={buttonDisabled}>{__("Sign In")}</Button>

                {/* <div className="signInForm__separator">
                    <div className="signInForm__separator__text">{__('OR')}</div>
                    <div className="signInForm__separator__line"></div>
                </div> */}
                {/* 
                <div className="signInForm__socials">
                    <FormGroup >
                        {fbbtn}
                    </FormGroup>
                    <FormGroup status={this.state.checkStatus} >
                        <Checkbox onChange={this.handleCheckboxValid}>
                            I certify that I am 18 years of age or older, and I agree to the <Link target="_blank" to={URL.Agreement}>User Agreement</Link>
                        </Checkbox>
                    </FormGroup>
                </div> */}


                <div className="signInForm__separator">
                    <div className="signInForm__separator__text">{__('OR')}</div>
                    <div className="signInForm__separator__line"></div>
                </div>
                <div className="signInForm__link">
                    <Link to={URL.SignUp} className="button link">{__('Sign Up')}</Link>
                </div>

            </div>
        )
    }
}

export default connect()(SignInForm)