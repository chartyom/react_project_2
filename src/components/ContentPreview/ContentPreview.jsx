import React, { PureComponent } from 'react'
import { connect } from 'react-redux';
//import * as ContentPreviewAction from '../../actions/ContentPreviewAction';
import { GetCurrentScroll } from '../../utils/UI'

class ContentPreview extends PureComponent {
    constructor() {
        super();
        this.state = {
            loaded: false
        };
        this.handleScroll = this.handleScroll.bind(this)
    }

    componentDidMount() {
        document.addEventListener('scroll', this.handleScroll, true);
    }

    componentWillUnmount() {
        document.removeEventListener('scroll', this.handleScroll, true);
    }

    handleScroll() {
        const currentScroll = GetCurrentScroll()
        const offsetTop = this.block.offsetTop
        const _innerHeight = innerHeight / 3
        const loaded = currentScroll + window.innerHeight - _innerHeight >= offsetTop &&
            currentScroll < offsetTop + this.block.offsetHeight - _innerHeight
        if (this.state.loaded == false) {
            this.setState({ loaded }, () => {
                if (this.props.callback) this.props.callback(loaded)
            })
        }
    }
    getData() {
        return this.props.ContentPreview;
    }
    rawMarkup() {
        var rawMarkup = this.getData().content
        return { __html: rawMarkup };
    }
    render() {
        var active = this.state.loaded ? " ContentPreview_in" : ""
        return (
            <div className={"ContentPreview" + active} ref={(div) => {
                this.block = div;
            }}>
                <div className="ContentPreview__container">
                    <div className="ContentPreview__item">
                        <div className="ContentPreview__content content">
                            <h1 className="content__header">{this.getData().title}</h1>
                            <div className="content__body" dangerouslySetInnerHTML={this.rawMarkup()}></div>
                            <div className="content__links">
                                <a href={this.getData().googleplay} target="_blank" title="GooglePlay" className="content__links__googleplay"></a>
                                <a href={this.getData().appstore} target="_blank" title="AppStore" className="content__links__appstore"></a>
                            </div>
                        </div>
                        <div className="ContentPreview__image">
                            <img src={this.getData().image} title={this.getData().title} alt={this.getData().title} className="ContentPreview__image__frame"></img>
                        </div>
                    </div>
                </div>
            </div>

        )
    }
}

const mapStateToProps = ({ ContentPreview }) => function () {
    return {
        ContentPreview: ContentPreview
    }
};

export default connect(mapStateToProps)(ContentPreview);
