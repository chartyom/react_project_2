import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import URL from "../../url"
import { ChangeTo } from "../../utils/LanguageHelper"
import ReactModal from 'react-modal';
import { connect } from 'react-redux';

class FooterMain extends Component {
    constructor() {
        super();
        this.state = {
            showModal: false
        };

        this.handleOpenModal = this.handleOpenModal.bind(this);
        this.handleCloseModal = this.handleCloseModal.bind(this);
        this.renderLanguagesList = this.renderLanguagesList.bind(this);
    }

    handleOpenModal() {
        this.setState({ showModal: true });
    }

    handleCloseModal() {
        this.setState({ showModal: false });
    }
    renderLanguagesList() {
        return LANGUAGES.map(function (v, i) {
            var activeClass = v.key == LANGUAGE ? " list__item__link--active" : ""
            return (
                <li key={i} className="list__item">
                    <div onClick={() => ChangeTo(v.key)} className={"list__item__link" + activeClass}>{v.value}</div>
                </li>
            )
        })
    }
    render() {
        return (
            <div className="FooterMain">
                <div className="FooterMain__container">
                    <div className="FooterMain__menu">
                        <ul className="FooterMain__menu__list">
                            <li className="FooterMain__menu__list__item">
                                <Link to={URL.Home} className="FooterMain__menu__list__item__link">{__("HOME")}</Link>
                            </li>
                            <li className="FooterMain__menu__list__item">
                                <a href={this.props.TokenSaleTerms} target="_blank" className="FooterMain__menu__list__item__link">{__("TOKEN SALE TERMS")}</a>
                            </li>
                            <li className="FooterMain__menu__list__item">
                                <Link to={URL.Contacts} className="FooterMain__menu__list__item__link">{__("CONTACTS")}</Link>
                            </li>
                        </ul>
                    </div>
                    <div className="FooterMain__right">
                        <div className="FooterMain__language">
                            <div className="FooterMain__language__button" onClick={this.handleOpenModal}>
                                {__("Language")}: <span>{LANGUAGE}</span>
                            </div>
                            <ReactModal
                                isOpen={this.state.showModal}
                                contentLabel="ChangeLanguage"
                                className="ModalWindow ModalWindow--small"
                                overlayClassName="ModalWindow__overlay"
                            >
                                <h4 className="ModalWindow__header">{__("Languages")}</h4>

                                <ul className="FooterMain__language__menu__list list">
                                    {this.renderLanguagesList()}
                                </ul>
                                <div className="ModalWindow__footer">
                                    <button
                                        onClick={this.handleCloseModal}
                                        className="ModalWindow__footer__button">
                                        {__("Close")}
                                    </button>
                                </div>

                            </ReactModal>
                        </div>
                        <div className="FooterMain__copyright">
                            Copyright 2017 © Bitkom
                        </div>
                    </div>
                </div>
            </div >
        )
    }
}

const mapStateToProps = ({ HeaderNavbar }) => function () {
    return {
        TokenSaleTerms: HeaderNavbar.TokenSaleTerms
    }
};

export default connect(mapStateToProps)(FooterMain);