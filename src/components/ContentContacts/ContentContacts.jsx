import React, { PureComponent } from 'react'
import { connect } from 'react-redux';

class ContentContacts extends PureComponent {
    getData() {
        return this.props.ContentContacts;
    }
    rawMarkup() {
        var rawMarkup = this.getData().content
        return { __html: rawMarkup };
    }
    render() {
        return (
            <div className="ContentContacts">
                <div className="ContentContacts__container">
                    <h3 className="ContentContacts__container__header">
                        {this.getData().title}
                    </h3>
                    <div className=" ContentContacts__content" dangerouslySetInnerHTML={this.rawMarkup()}></div>
                </div>
            </div>

        )
    }
}

const mapStateToProps = ({ ContentContacts }) => function () {
    return {
        ContentContacts: ContentContacts
    }
};

export default connect(mapStateToProps)(ContentContacts);
