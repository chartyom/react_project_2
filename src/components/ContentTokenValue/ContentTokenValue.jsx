import React, { PureComponent } from 'react'
import { connect } from 'react-redux';

class ContentTokenValue extends PureComponent {
    getData() {
        return this.props.ContentTokenValue;
    }
    rawMarkupInfo() {
        var rawMarkup = this.getData().info
        return { __html: rawMarkup };
    }
    rawMarkupICO() {
        var rawMarkup = this.getData().ico.content
        return { __html: rawMarkup };
    }
    rawMarkupBonuses() {
        var rawMarkup = this.getData().bonuses.content
        return { __html: rawMarkup };
    }
    render() {
        return (
            <div className="ContentTokenValue">
                <div className="ContentTokenValue__container">
                    <h3 className="ContentTokenValue__container__header">
                        {this.getData().title}
                    </h3>
                    <div className=" ContentTokenValue__content info">
                        <div className="info__container">
                            <div className="info__content" dangerouslySetInnerHTML={this.rawMarkupInfo()}></div>
                        </div>
                    </div>
                    <div className=" ContentTokenValue__content data">
                        <div className="data__container">
                            <div className="data__content" dangerouslySetInnerHTML={this.rawMarkupICO()}></div>
                        </div>
                    </div>
                    <div className=" ContentTokenValue__content bonuses">
                        <div className="bonuses__container">
                            <div className="bonuses__header">{this.getData().bonuses.title}</div>
                            <div className="bonuses__content" dangerouslySetInnerHTML={this.rawMarkupBonuses()}></div>
                        </div>
                    </div>
                    <a href={this.getData().image} target="_blank" title="Open in new tab">
                        <img src={this.getData().image} alt={this.getData().title} />
                    </a>
                </div>
            </div>

        )
    }
}

const mapStateToProps = ({ ContentTokenValue }) => function () {
    return {
        ContentTokenValue: ContentTokenValue
    }
};

export default connect(mapStateToProps)(ContentTokenValue);
