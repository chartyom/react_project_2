import React, { PureComponent } from 'react'

class SocialLinks extends PureComponent {
    render() {
        return (
            <div className="SocialLinks">
                <div className="SocialLinks__container">
                    <div className="SocialLinks__content">
                        <a href="//twitter.com/bitkom_io" className="SocialLinks__content__item SocialLinks__content__item--t" target="_blank" title="Go to Twitter"></a>
                        <a href="//fb.me/bitkom.io" className="SocialLinks__content__item SocialLinks__content__item--f" target="_blank" title="Go to Facebook"></a>
                        <a href="//instagram.com/bitkom.io" className="SocialLinks__content__item SocialLinks__content__item--i" target="_blank" title="Go to Instagram"></a>
                        <a href="//bitkomio.slack.com/" className="SocialLinks__content__item SocialLinks__content__item--s" target="_blank" title="Go to Slack"></a>
                        <a href="//t.me/joinchat/B0PqQQ1MqSxesCkI5QLilw" className="SocialLinks__content__item SocialLinks__content__item--tg" target="_blank" title="Joint to chat"></a>
                    </div>
                </div>
            </div>

        )
    }
}

export default SocialLinks;
