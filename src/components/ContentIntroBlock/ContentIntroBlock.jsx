import React, { PureComponent } from 'react'
import { connect } from 'react-redux';

class ContentIntroBlock extends PureComponent {
    getData() {
        return this.props.ContentIntroBlock;
    }
    rawMarkup() {
        var rawMarkup = this.getData().content
        return { __html: rawMarkup };
    }
    render() {
        return (
            <div className="ContentIntroBlock">
                <div className="ContentIntroBlock__container">
                    <div className="ContentIntroBlock__left">
                        <h3 className=" ContentIntroBlock__container__header">
                            {this.getData().title}
                        </h3>
                        <div className=" ContentIntroBlock__content" dangerouslySetInnerHTML={this.rawMarkup()}></div>
                    </div>
                    <div className="ContentIntroBlock__image">
                    </div>
                </div>
            </div>

        )
    }
}

const mapStateToProps = ({ ContentIntroBlock }) => function () {
    return {
        ContentIntroBlock: ContentIntroBlock
    }
};

export default connect(mapStateToProps)(ContentIntroBlock);
