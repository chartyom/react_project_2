import React, { PureComponent } from 'react'
import { connect } from 'react-redux';
import { GetCurrentScroll } from '../../utils/UI'

class ContentMainAdvantages extends PureComponent {
    constructor() {
        super();
        this.state = {
            loaded: false
        };
        this.handleScroll = this.handleScroll.bind(this)
    }

    componentDidMount() {
        document.addEventListener('scroll', this.handleScroll, true);
    }

    componentWillUnmount() {
        document.removeEventListener('scroll', this.handleScroll, true);
    }

    handleScroll() {
        const currentScroll = GetCurrentScroll()
        const offsetTop = this.block.offsetTop
        const _innerHeight = innerHeight / 3
        const loaded = currentScroll + window.innerHeight - _innerHeight >= offsetTop &&
            currentScroll < offsetTop + this.block.offsetHeight - _innerHeight
        if (this.state.loaded == false) {
            this.setState({ loaded }, () => {
                if (this.props.callback) this.props.callback(loaded)
            })
        }
    }
    getData() {
        return this.props.ContentMainAdvantages;
    }
    rawMarkup1() {
        var rawMarkup = this.getData().business.content
        return { __html: rawMarkup };
    }
    rawMarkup2() {
        var rawMarkup = this.getData().users.content
        return { __html: rawMarkup };
    }
    render() {
        var active = this.state.loaded ? " ContentMainAdvantages_in" : ""
        return (
            <div className={"ContentMainAdvantages" + active} ref={(div) => {
                this.block = div;
            }}>
                <div className="ContentMainAdvantages__container">
                    <h3 className="ContentMainAdvantages__container__header">
                        {this.getData().title}
                    </h3>
                    <div className="ContentMainAdvantages__content list">
                        <div className="list__item">
                            <h4 className="list__item__header">{this.getData().business.title}</h4>
                            <div className="list__item__content" dangerouslySetInnerHTML={this.rawMarkup1()}></div>
                        </div>
                        <div className="list__item">
                            <h4 className="list__item__header">{this.getData().users.title}</h4>
                            <div className="list__item__content" dangerouslySetInnerHTML={this.rawMarkup2()}></div>
                        </div>
                    </div>
                </div>
            </div>

        )
    }
}

const mapStateToProps = ({ ContentMainAdvantages }) => function () {
    return {
        ContentMainAdvantages: ContentMainAdvantages
    }
};

export default connect(mapStateToProps)(ContentMainAdvantages);
