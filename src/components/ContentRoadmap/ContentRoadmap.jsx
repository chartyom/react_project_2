import React, { PureComponent } from 'react'
import { Link } from 'react-router-dom'
import { connect } from 'react-redux';

class ContentRoadmap extends PureComponent {
    getData() {
        return this.props.ContentRoadmap;
    }

    render() {
        return (
            <div className="ContentRoadmap">
                <div className="ContentRoadmap__container">
                    <h3 className="ContentRoadmap__container__header">{this.getData().title}</h3>
                    <div className="ContentRoadmap__element">
                        <a href={this.getData().firstSteps} target="_blank" title="Open in new tab">
                            <img src={this.getData().firstSteps} alt="First steps" />
                        </a>
                    </div>
                    <div className="ContentRoadmap__element">
                        <a href={this.getData().secondSteps} target="_blank" title="Open in new tab">
                            <img src={this.getData().secondSteps} alt="Second steps" />
                        </a>
                    </div>
                </div>
            </div>

        )
    }
}



const mapStateToProps = ({ ContentRoadmap }) => function () {
    return {
        ContentRoadmap: ContentRoadmap
    }
};

export default connect(mapStateToProps)(ContentRoadmap);
