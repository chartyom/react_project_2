import React, { Component } from 'react'
import { Link, Redirect } from 'react-router-dom'
import URL from '../../url'
import { AddWallet } from '../../actions/userAction'
// import * as uiAction from '../actions/uiAction'
import matchesValidator from 'validator/lib/matches'
import isEmailValidator from 'validator/lib/isEmail';
import { connect } from 'react-redux';
import { FormGroup, Field, FieldHelp, Button } from '../elements'
import API from '../../api'

/**
 * 
 * @param {Function} handleSetReferrer
 */
class AccountForm extends Component {

    constructor(props) {
        super(props);
        this.state = {
            ethValue: "",
            ethLoading: false,
            ethStatus: null,

            etcValue: "",
            etcLoading: false,
            etcStatus: null,

            btcValue: "",
            btcLoading: false,
            btcStatus: null,

            bchValue: "",
            bchLoading: false,
            bchStatus: null,

            ltcValue: "",
            ltcLoading: false,
            ltcStatus: null,

            dashValue: "",
            dashLoading: false,
            dashStatus: null,

            copywallet: __('Copy wallet'),
            copywalletState: "DEFAULT",
        };

        this.handleEthValid = this.handleEthValid.bind(this)
        this.handleEtcValid = this.handleEtcValid.bind(this)
        this.handleBtcValid = this.handleBtcValid.bind(this)
        this.handleBchValid = this.handleBchValid.bind(this)
        this.handleLtcValid = this.handleLtcValid.bind(this)
        this.handleDashValid = this.handleDashValid.bind(this)
        this.handleSuccess = this.handleSuccess.bind(this)
        this.handleCopy = this.handleCopy.bind(this)
        this.handleWalletChange = this.handleWalletChange.bind(this)
    }

    componentDidMount() {
        this.setState({
            ethValue: this.getUser().ethWallet,
            etcValue: this.getUser().etcWallet,
            btcValue: this.getUser().btcWallet,
            bchValue: this.getUser().bchWallet,
            ltcValue: this.getUser().ltcWallet,
            dashValue: this.getUser().dashWallet,
        })
    }

    getAF() {
        return this.props.AccountForm
    }

    getUser() {
        return this.props.user
    }

    handleEthValid(e) {
        var rgx = /^0x[0-9a-fA-F]{40}$/ //0x6fC21092DA55B392b045eD78F4732bff3C580e2c
        var v = e.target.value
        if (v && v.length > 0) {
            if (v && rgx.test(v)) {
                this.setState({ ethValue: v, ethStatus: "success" })
            } else {
                this.setState({ ethValue: v, ethStatus: "danger" })
            }
        } else {
            this.setState({ ethValue: v, ethStatus: null })
        }
    }

    handleEtcValid(e) {
        var v = e.target.value
        if (v && v.length > 0) {
            this.setState({ etcValue: v, etcStatus: "success" })
        } else {
            this.setState({ etcValue: v, etcStatus: null })
        }
    }

    handleBtcValid(e) {
        var v = e.target.value
        if (v && v.length > 0) {
            this.setState({ btcValue: v, btcStatus: "success" })
        } else {
            this.setState({ btcValue: v, btcStatus: null })
        }
    }

    handleBchValid(e) {
        var v = e.target.value
        if (v && v.length > 0) {
            this.setState({ bchValue: v, bchStatus: "success" })
        } else {
            this.setState({ bchValue: v, bchStatus: null })
        }
    }

    handleLtcValid(e) {
        var v = e.target.value
        if (v && v.length > 0) {
            this.setState({ ltcValue: v, ltcStatus: "success" })
        } else {
            this.setState({ ltcValue: v, ltcStatus: null })
        }
    }

    handleDashValid(e) {
        var v = e.target.value
        if (v && v.length > 0) {
            this.setState({ dashValue: v, dashStatus: "success" })
        } else {
            this.setState({ dashValue: v, dashStatus: null })
        }
    }

    handleSuccess(type) {
        var type, wallet, save = this.getUser(), loader = {}
        switch (type) {
            case "eth":
                if (this.state.ethStatus == 'success') {
                    type = 'ethWallet'
                    wallet = this.state.ethValue
                    this.setState({ ethLoading: true })
                    loader = { ethLoading: false }
                    save.ethWallet = wallet
                } else {
                    this.setState({ ethStatus: 'danger' })
                }
                break
            case "etc":
                if (this.state.etcStatus == 'success') {
                    type = 'etcWallet'
                    wallet = this.state.etcValue
                    this.setState({ etcLoading: true })
                    loader = { etcLoading: false }
                    save.etcWallet = wallet
                } else {
                    this.setState({ etcStatus: 'danger' })
                }
                break
            case "btc":
                if (this.state.btcStatus == 'success') {
                    type = 'btcWallet'
                    wallet = this.state.btcValue
                    this.setState({ btcLoading: true })
                    loader = { btcLoading: false }
                    save.btcWallet = wallet
                } else {
                    this.setState({ btcStatus: 'danger' })
                }
                break
            case "bch":
                if (this.state.bchStatus == 'success') {
                    type = 'bchWallet'
                    wallet = this.state.bchValue
                    this.setState({ bchLoading: true })
                    loader = { bchLoading: false }
                    save.bchWallet = wallet
                } else {
                    this.setState({ bchStatus: 'danger' })
                }
                break
            case "ltc":
                if (this.state.ltcStatus == 'success') {
                    type = 'ltcWallet'
                    wallet = this.state.ltcValue
                    this.setState({ ltcLoading: true })
                    loader = { ltcLoading: false }
                    save.ltcWallet = wallet
                } else {
                    this.setState({ ltcStatus: 'danger' })
                }
                break
            case "dash":
                if (this.state.dashStatus == 'success') {
                    type = 'dashWallet'
                    wallet = this.state.dashValue
                    this.setState({ dashLoading: true })
                    loader = { dashLoading: false }
                    save.dashWallet = wallet
                } else {
                    this.setState({ dashStatus: 'danger' })
                }
                break
        }
        if (!type || !wallet) return

        API.user.AddWallet(type, wallet).then((res) => {
            this.setState(loader)
            if (!res || res.error) {
                return console.log(res.error);
            }
            if (!res.ok) {
                return console.log("Error API.user.AddWallet", type, wallet);
            }
            this.props.dispatch(AddWallet(save))
        }).catch((e) => {
            this.setState(loader)
            return console.log(e);
        })
    }

    handleCopy(type) {
        console.log('handleCopy: .copywallet__item__input_' + type)
        var txt = document.querySelector('.copywallet__item__input_' + type);
        //console.log(txt)
        txt.select();
        // var range = document.createRange();
        // range.selectNode(txt);
        // window.getSelection().addRange(range);
        try {
            var successful = document.execCommand('copy');
            var msg = successful ? 'successful' : 'unsuccessful';
            console.log('Copy wallet command was ' + msg);
        } catch (err) {
            console.log('Oops, unable to copy');
        }
    }

    handleWalletChange() {

    }

    render() {
        return (
            <div className="AccountForm">
                <h1 className="AccountForm__title">{this.getAF().title}</h1>
                <div className="AccountForm__container">
                    {this.renderAddWallet()}
                    {this.renderCopyWallet()}
                    {this.renderAwait()}
                </div>
            </div>
        )
    }
    renderAddWallet() {
        var ethButtonDisabled = false
        if (this.state.ethLoading) ethButtonDisabled = true

        var etcButtonDisabled = false
        if (this.state.etcLoading) etcButtonDisabled = true

        var btcButtonDisabled = false
        if (this.state.btcLoading) btcButtonDisabled = true

        var bchButtonDisabled = false
        if (this.state.bchLoading) bchButtonDisabled = true

        var ltcButtonDisabled = false
        if (this.state.ltcLoading) ltcButtonDisabled = true

        var dashButtonDisabled = false
        if (this.state.dashLoading) dashButtonDisabled = true

        return (
            <div className="AccountForm__container__item addwallet">
                <div className="AccountForm__container__item__header">
                    1. {this.getAF().addwallet.title}
                </div>
                <div className="addwallet__list">
                    <FormGroup status={this.state.ethStatus} className="addwallet__item">
                        <div className="addwallet__item__symbol">ETH<span>*</span></div>
                        <Field className="addwallet__item__input" placeholder={__("Enter eth wallet")} type="text" onChange={this.handleEthValid} value={this.state.ethValue} />
                        {this.getUser().ethWallet ?
                            <Button className={"success addwallet__item__button" + (this.state.ethLoading ? ' loading' : '')} onClick={this.handleSuccess.bind(this, "eth")} disabled={ethButtonDisabled}>{__('EDIT')}</Button> :
                            <Button className={"primary addwallet__item__button" + (this.state.ethLoading ? ' loading' : '')} onClick={this.handleSuccess.bind(this, "eth")} disabled={ethButtonDisabled}>{__('ADD')}</Button>}
                    </FormGroup>
                    <FormGroup status={this.state.etcStatus} className="addwallet__item">
                        <div className="addwallet__item__symbol">ETC</div>
                        <Field className="addwallet__item__input" placeholder={__("Enter etc wallet")} type="text" onChange={this.handleEtcValid} value={this.state.etcValue} />
                        {this.getUser().etcWallet ?
                            <Button className={"success addwallet__item__button" + (this.state.etcLoading ? ' loading' : '')} onClick={this.handleSuccess.bind(this, "etc")} disabled={etcButtonDisabled}>{__('EDIT')}</Button> :
                            <Button className={"primary addwallet__item__button" + (this.state.etcLoading ? ' loading' : '')} onClick={this.handleSuccess.bind(this, "etc")} disabled={etcButtonDisabled}>{__('ADD')}</Button>}
                    </FormGroup>
                    <FormGroup status={this.state.btcStatus} className="addwallet__item">
                        <div className="addwallet__item__symbol">BTC</div>
                        <Field className="addwallet__item__input" placeholder={__("Enter btc wallet")} type="text" onChange={this.handleBtcValid} value={this.state.btcValue} />
                        {this.getUser().btcWallet ?
                            <Button className={"success addwallet__item__button" + (this.state.btcLoading ? ' loading' : '')} onClick={this.handleSuccess.bind(this, "btc")} disabled={btcButtonDisabled}>{__('EDIT')}</Button> :
                            <Button className={"primary addwallet__item__button" + (this.state.btcLoading ? ' loading' : '')} onClick={this.handleSuccess.bind(this, "btc")} disabled={btcButtonDisabled}>{__('ADD')}</Button>}
                    </FormGroup>
                    <FormGroup status={this.state.ltcStatus} className="addwallet__item">
                        <div className="addwallet__item__symbol">LTC</div>
                        <Field className="addwallet__item__input" placeholder={__("Enter ltc wallet")} type="text" onChange={this.handleLtcValid} value={this.state.ltcValue} />
                        {this.getUser().ltcWallet ?
                            <Button className={"success addwallet__item__button" + (this.state.ltcLoading ? ' loading' : '')} onClick={this.handleSuccess.bind(this, "ltc")} disabled={ltcButtonDisabled}>{__('EDIT')}</Button> :
                            <Button className={"primary addwallet__item__button" + (this.state.ltcLoading ? ' loading' : '')} onClick={this.handleSuccess.bind(this, "ltc")} disabled={ltcButtonDisabled}>{__('ADD')}</Button>}
                    </FormGroup>
                    <FormGroup status={this.state.bchStatus} className="addwallet__item">
                        <div className="addwallet__item__symbol">BCH</div>
                        <Field className="addwallet__item__input" placeholder={__("Enter bch wallet")} type="text" onChange={this.handleBchValid} value={this.state.bchValue} />
                        {this.getUser().bchWallet ?
                            <Button className={"success addwallet__item__button" + (this.state.bchLoading ? ' loading' : '')} onClick={this.handleSuccess.bind(this, "bch")} disabled={bchButtonDisabled}>{__('EDIT')}</Button> :
                            <Button className={"primary addwallet__item__button" + (this.state.bchLoading ? ' loading' : '')} onClick={this.handleSuccess.bind(this, "bch")} disabled={bchButtonDisabled}>{__('ADD')}</Button>}
                    </FormGroup>
                    <FormGroup status={this.state.dashStatus} className="addwallet__item">
                        <div className="addwallet__item__symbol">DASH</div>
                        <Field className="addwallet__item__input" placeholder={__("Enter dash wallet")} type="text" onChange={this.handleDashValid} value={this.state.dashValue} />
                        {this.getUser().dashWallet ?
                            <Button className={"success addwallet__item__button" + (this.state.dashLoading ? ' loading' : '')} onClick={this.handleSuccess.bind(this, "dash")} disabled={dashButtonDisabled}>{__('EDIT')}</Button> :
                            <Button className={"primary addwallet__item__button" + (this.state.dashLoading ? ' loading' : '')} onClick={this.handleSuccess.bind(this, "dash")} disabled={dashButtonDisabled}>{__('ADD')}</Button>}
                    </FormGroup>
                </div>
            </div>
        )
    }

    renderCopyWallet() {
        var CN = this.getUser().ethWallet ? "" : " copywallet_silent"
        return (
            <div className={"AccountForm__container__item copywallet" + CN}>
                <div className="AccountForm__container__item__header">
                    2. {this.getAF().copywallet.title}
                </div>
                {/* <div className="copywallet__content" dangerouslySetInnerHTML={{ __html: this.getAF().copywallet.content }}></div> */}
                <FormGroup className="copywallet__button">
                    <a href={this.props.TokenSaleTerms} target="_blank">{__('TOKEN SALE TERMS')}</a>
                </FormGroup>
                <div className="copywallet__description">{this.getAF().copywallet.description}</div>
                <div className="copywallet__list">
                    {this.getAF().ethWallet ? <div className="copywallet__item">
                        <div className="copywallet__item__symbol">ETH:</div>
                        <input autoComplete="off" type="text" className="copywallet__item__input copywallet__item__input_eth" value={this.getAF().ethWallet} />
                        <button type="button" className="copywallet__item__button" title="Copy wallet" onClick={this.handleCopy.bind(this, 'eth')}></button>
                    </div> : ""}
                    {this.getAF().etcWallet ? <div className="copywallet__item">
                        <div className="copywallet__item__symbol">ETC:</div>
                        <input autoComplete="off" type="text" className="copywallet__item__input copywallet__item__input_etc" value={this.getAF().etcWallet} />
                        <button type="button" className="copywallet__item__button" title="Copy wallet" onClick={this.handleCopy.bind(this, 'etc')}></button>
                    </div> : ""}
                    {this.getAF().btcWallet ? <div className="copywallet__item">
                        <div className="copywallet__item__symbol">BTC:</div>
                        <input autoComplete="off" type="text" className="copywallet__item__input copywallet__item__input_btc" value={this.getAF().btcWallet} />
                        <button type="button" className="copywallet__item__button" title="Copy wallet" onClick={this.handleCopy.bind(this, 'btc')}></button>
                    </div> : ""}
                    {this.getAF().bchWallet ? <div className="copywallet__item">
                        <div className="copywallet__item__symbol">BCH:</div>
                        <input autoComplete="off" type="text" className="copywallet__item__input copywallet__item__input_bch" value={this.getAF().bchWallet} />
                        <button type="button" className="copywallet__item__button" title="Copy wallet" onClick={this.handleCopy.bind(this, 'bch')}></button>
                    </div> : ""}
                    {this.getAF().ltcWallet ? <div className="copywallet__item">
                        <div className="copywallet__item__symbol">LTC:</div>
                        <input autoComplete="off" type="text" className="copywallet__item__input copywallet__item__input_ltc" value={this.getAF().ltcWallet} />
                        <button type="button" className="copywallet__item__button" title="Copy wallet" onClick={this.handleCopy.bind(this, 'ltc')}></button>
                    </div> : ""}
                    {this.getAF().dashWallet ? <div className="copywallet__item">
                        <div className="copywallet__item__symbol">DASH:</div>
                        <input autoComplete="off" type="text" className="copywallet__item__input copywallet__item__input_dash" value={this.getAF().dashWallet} />
                        <button type="button" className="copywallet__item__button" title="Copy wallet" onClick={this.handleCopy.bind(this, 'dash')}></button>
                    </div> : ""}
                </div>
            </div>
        )
    }
    renderAwait() {
        var CN = this.getUser().ethWallet ? "" : " await_silent"
        return (
            <div className={"AccountForm__container__item await" + CN}>
                <div className="AccountForm__container__item__header">
                    3. {this.getAF().await.title}
                </div>
                <div className="await__content" dangerouslySetInnerHTML={{ __html: this.getAF().await.content }}></div>
                <div className="await__description" dangerouslySetInnerHTML={{ __html: this.getAF().await.description }}></div>
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    AccountForm: state.AccountForm,
    user: state.User.user,
    TokenSaleTerms: state.HeaderNavbar.TokenSaleTerms
})


export default connect(mapStateToProps)(AccountForm)