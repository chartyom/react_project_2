import React, { PureComponent } from 'react'
import { GetCurrentScroll } from '../../utils/UI'

var ContentBrandsList = [
    {
        photo: "/images/brands_1.png",
        name: "Maison Dellos",
    },
    {
        photo: "/images/brands_2.png",
        name: "Hard Rock Café",
    },
    {
        photo: "/images/brands_3.png",
        name: "Novikov Group",
    },
    {
        photo: "/images/brands_4.png",
        name: "Buddha bar",
    },
    {
        photo: "/images/brands_5.png",
        name: "Ginza Project",
    },
    {
        photo: "/images/brands_6.png",
        name: "Coyote Ugly",
    },
    {
        photo: "/images/brands_7.png",
        name: "Papa John’s",
    },
    {
        photo: "/images/brands_8.png",
        name: "Domino’s Pizza",
    },
    {
        photo: "/images/brands_9.png",
        name: "Теремок",
    },
]

class ContentBrands extends PureComponent {
    constructor() {
        super();
        this.state = {
            loaded: false
        };
        this.handleScroll = this.handleScroll.bind(this)
    }

    componentDidMount() {
        document.addEventListener('scroll', this.handleScroll, true);
    }

    componentWillUnmount() {
        document.removeEventListener('scroll', this.handleScroll, true);
    }

    handleScroll() {
        const currentScroll = GetCurrentScroll()
        const offsetTop = this.block.offsetTop
        const _innerHeight = innerHeight / 3
        const loaded = currentScroll + window.innerHeight - _innerHeight >= offsetTop &&
            currentScroll < offsetTop + this.block.offsetHeight - _innerHeight
        if (this.state.loaded == false) {
            this.setState({ loaded }, () => {
                if (this.props.callback) this.props.callback(loaded)
            })
        }
    }

    getData() {
        return this.props.ContentBrands;
    }

    handleList() {
        return ContentBrandsList.map(function (v, i) {
            return (
                <div key={i} className="ContentBrands__list__item"><img src={v.photo} alt={v.name} title={v.name} /></div>
            )
        })
    }

    render() {
        var active = this.state.loaded ? " ContentBrands_in" : ""
        return (
            <div className={"ContentBrands" + active} ref={(div) => {
                this.block = div;
            }}>
                <div className="ContentBrands__container">
                    <div className="ContentBrands__list">
                        {this.handleList()}
                    </div>
                </div>
            </div>

        )
    }
}



export default ContentBrands;
