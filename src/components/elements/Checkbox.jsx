import React from 'react';

class Checkbox extends React.Component {

    render() {
        const {
            disabled,
            className,
            id,
            defaultChecked,
            onChange
        } = this.props;
        var cn = className ? ' ' + className : '';
        var idt = id ? id : 'checkbox' + Math.floor(Math.random() * 1001);
        return (
            <div className={'Checkbox' + cn}>
                <input id={idt} type="checkbox" defaultChecked={defaultChecked} disabled={disabled} onChange={onChange} />
                <label htmlFor={idt} >{this.props.children}</label>
            </div>
        )

    }
}

export default Checkbox;