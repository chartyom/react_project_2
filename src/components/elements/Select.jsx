import React from 'react';

class Select extends React.Component {
    render() {
        const {
            disabled,
            className,
            list,
            onChange
        } = this.props;
        const CN = className ? ' ' + className : '';

        return (
            <div className='Select__parent'>
                <select className={"Select form__field" + CN} onChange={onChange} disabled={disabled}>{list.map((v, i) => (<option key={i} value={v}>{v}</option>))}</select>
            </div>
        )

    }
}

export default Select;