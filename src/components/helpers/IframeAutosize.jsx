import React, { PureComponent } from 'react';

class IframeAutosize extends PureComponent {

    render() {
        const { className, classNameIframe, ...props } = this.props;
        const s2 = classNameIframe ? 'IframeAutosize__iframe ' + classNameIframe : 'IframeAutosize__iframe';
        const s1 = className ? 'IframeAutosize ' + className : 'IframeAutosize';
        return (
            <div className={s1} >
                < iframe {...props } className={s2} gesture="media" frameBorder="0" allow="encrypted-media" allowFullScreen></iframe>
            </div>
        )

    }
}

export default IframeAutosize;