global.ENVIRONMENT = process.env.NODE_ENV || 'local';
global.DEVELOPMENT = process.env.NODE_ENV != 'production' ? true : false;

const express = require('express');
const config = require('./config');
const error = require('./utils/errors');
const routes = require('./routes');
const api = require('./api');
const helmet = require('helmet');
const hbs = require('hbs')
const path = require('path')
const cookieParser = require('cookie-parser')
const bodyParser = require('body-parser')
const app = express();

app.use(cookieParser())
app.use(bodyParser.json())
app.use(helmet({
    frameguard: {
        action: 'deny'
    },
    hsts: false,
    ieNoOpen: false
}));

// app.get('/whitepaper/:lang', (req, res, next) => {
//     res.sendFile('./files')
// })

// app.get('/onepager/:lang', (req, res, next) => {
//     req.file('./files')
// })

app.use('/api', api)

// view engine setup
app.set('view engine', 'hbs');
app.set('views', path.join(__dirname, 'views'));

// dir public files setub
app.use(express.static(path.join(__dirname, 'public')))

routes(app)

app.use(function (req, res, next) {
    res.status(404).jsonp({ error: error.NotFound, message: error.NotFound });
});

app.use(function (err, req, res, next) {
    if (DEVELOPMENT) {
        console.log(err)
        res.status(err.status || 500).json({
            error: error.InternalServerError,
            message: err
        });
    } else {
        res.status(err.status || 500).json({
            error: error.InternalServerError,
            message: error.InternalServerError
        });
    }
});

// Start listening for requests.
const port = config.server.port;
const host = config.server.host;

app.listen(port, (err) => {
    if (err) return console.error(err);
    console.info(`⚡ Server running on http://${host}:${port}/ ⚡ environment:`, ENVIRONMENT);
});