const router = require('express').Router()
const { checkToken, timelineRule } = require('../middlewares')
const User = require('../models/user')
const ReqGetLang = require('../../utils/language').ReqGetLang
const Languages = require('../../languages')

// /api/user
router.get('/', checkToken, async (req, res) => {
  try {
    let user = await User.findOne({ email: req.user.email }).select('email firstName lastName emailVerified ethWallet etcWallet btcWallet ltcWallet bchWallet dashWallet')
    if (!user) {
      res.status(200).json({ ok: false, message: 'Bad user email', user: null })
    } else {
      res.status(200).json({ ok: true, user })
    }
  } catch (error) {
    res.status(500).json({ ok: false, user: null })
  }
})

// /api/user/bitkomwallets
router.get('/bitkomwallets', timelineRule, checkToken, async (req, res) => {
  try {
    let user = await User.findOne({ email: req.user.email, emailVerified: true }).select('email emailVerified ethWallet')
    if (!user) {
      res.status(200).json({ ok: false, message: 'Bad user email', bitkomwallets: null })
    } else if (!user.emailVerified || user.ethWallet === null) {
      res.status(200).json({ ok: false, message: `User doesn't verified or ethWallet field is empty`, bitkomwallets: null })
    } else if (user.emailVerified && user.ethWallet != null) {
      res.status(200).json({
        ok: true,
        bitkomwallets: {
          ethWallet: '',  // TODO
          etcWallet: '',  // TODO
          btcWallet: '',  // TODO
          bchWallet: '',  // TODO
          dashWallet: '', // TODO
          ltcWallet: '' // TODO
        }
      })
    }
  } catch (error) {
    res.status(500).json({ ok: false, user: null })
  }
})
// /api/user/wallet
// router.post('/wallet', checkToken, async (req, res, next) => {
//   try {
//     let user = await User.findOne({ email: req.user.email, emailVerified: true })
//     if (!user) {
//       res.status(200).json({ ok: false, message: 'User email does not verified' })
//       next()
//     }

//     let userWithTheSameWallet = await User.findOne({
//       Wallet: req.body.wallet
//     })
//     if (userWithTheSameWallet) {
//       res.status(200).json({ ok: false, message: 'This wallet already in use' })
//       next()
//     } else {
//       user[`${req.params.type}Wallet`] = req.body.wallet
//       await user.save()
//       res.status(200).json({ ok: true })
//     }

//   } catch (error) {
//     res.status(500).json({ ok: false })
//   }
// })

// /api/user/addwallet/:type OneOf ['ethWallet', 'etcWallet', 'btcWallet', 'bchWallet', 'dashWallet', 'ltcWallet']
router.post('/addwallet/:type', checkToken, async (req, res, next) => {
  try {
    let user = await User.findOne({ email: req.user.email, emailVerified: true })
    if (!user) {
      res.status(200).json({ ok: false, message: 'User email does not verified' })
      return
    }

    if (['ethWallet', 'etcWallet', 'btcWallet', 'bchWallet', 'dashWallet', 'ltcWallet'].indexOf(req.params.type) > -1) {
      let userWithTheSameWallet = await User.findOne({
        [req.params.type]: req.body.wallet,
        _id: { $ne: req.user.id }
      })
      if (userWithTheSameWallet) {
        res.status(200).json({ ok: false, message: 'This wallet already in use' })
        return
      } else {
        user[req.params.type] = req.body.wallet
        await user.save()
        res.status(200).json({ ok: true })
      }

    } else {
      res.status(200).json({ ok: false, message: 'Bad wallet type' })
    }

  } catch (error) {
    res.status(500).json({ ok: false })
  }
})

// /api/user/agreement
router.get('/agreement', async (req, res) => {
  try {
    var lang = ReqGetLang(req)
    var data = Languages()[lang].AgreementContent
    res.status(200).json({
      agreement: data
    })
  } catch (error) {
    res.status(500).json({ ok: false, error })
  }
})

module.exports = router