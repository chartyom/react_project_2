const router = require('express').Router()
const jwt = require('jsonwebtoken')
const Axios = require('axios')
const Request = require('request-promise')
const uuidV4 = require('uuid/v4')
const generator = require('generate-password')
const Languages = require('../../languages')
const ReqGetLang = require('../../utils/language').ReqGetLang

const Email = require('../email')
const User = require('../models/user')
const config = require('../../config')

const MaxAgeForCookieJWT = 1000 * 60 * 60 * 24 * 180 // 180 days = 15552000000

function makeJwt(user) {
  return jwt.sign({ id: user._id, email: user.email }, config.secret, { expiresIn: '180d' })
}

// /api/auth/signin
router.post('/signin', async (req, res) => {
  const { email, password } = req.body
  try {
    const user = await User.findOne({ email, emailVerified: true })
    if (user) {
      let isCorrectPassword = await user.comparePassword(password)
      if (isCorrectPassword) {
        let jwtPayload = makeJwt(user)
        res.status(200).json({ ok: true, token: jwtPayload })
      } else {
        res.status(200).json({ ok: false, token: null })
      }
    } else {
      res.status(200).json({ ok: false, token: null })
    }
  } catch (error) {
    console.log(error);
    res.status(500).json({ ok: false, token: null })
  }
})

// /api/auth/registration
router.post('/registration', async (req, res) => {
  try {
    const { email, password, country } = req.body
    let userWithProvidedEmail = await User.findOne({ email, emailVerified: true })
    if (!userWithProvidedEmail) {
      let user = new User({ email, password, country })
      user.emailToken = uuidV4()
      user = await user.save()
      await Email.sendRegistrationEmail({ email: user.email, emailToken: user.emailToken })
      res.status(200).json({ ok: true })
    } else {
      await Email.sendDublicateEmail({ email })
      res.status(200).json({ ok: true })
    }
  } catch (error) {
    console.log(error)
    res.status(500).json({ ok: false })
  }
})

// /api/auth/countries
router.get('/countries', async (req, res) => {
  try {
    var lang = ReqGetLang(req)
    var data = Languages()[lang].AccessCountries
    res.status(200).json({
      countries: data
    })
  } catch (error) {
    res.status(500).json({ ok: false, error })
  }
})

// /api/auth/wallets
router.get('/wallets', async (req, res) => {
  try {
    var data = ['ethWallet', 'etcWallet', 'btcWallet', 'bchWallet', 'dashWallet', 'ltcWallet']
    res.status(200).json({
      wallets: data
    })
  } catch (error) {
    res.status(500).json({ ok: false, error })
  }
})


// /api/auth/facebook/callback
// router.get('/facebook/callback', async (req, res) => {
//   try {
//     const { code, redirectUri } = req.query
//     if (!code) {
//       res.status(500).json({ ok: false, message: 'Field code is empty' })
//     }
//     const response = await Axios.get('https://graph.facebook.com/v2.11/oauth/access_token', {
//       params: {
//         client_id: config.server.oauth.facebook.clientId,
//         client_secret: config.server.oauth.facebook.clientSecret,
//         code: code,
//         redirect_uri: redirectUri
//       }
//     })
//     let fbResponse = await Axios.get('https://graph.facebook.com/v2.11/me?fields=name,email,birthday,picture', {
//       params: { access_token: response.data.access_token }
//     })
//     const userInFacebook = fbResponse.data
//     if (!userInFacebook.id) {
//       console.log(userInFacebook)
//       throw Error('No id in fb response')
//     }
//     let user = await User.findOne({ facebookId: userInFacebook.id })
//     if (!user) {
//       let password = generator.generate({ numbers: true, excludeSimilarCharacters: true })
//       user = new User({
//         email: userInFacebook.email,
//         emailVerified: true,
//         firstName: userInFacebook.first_name,
//         lastName: userInFacebook.last_name,
//         facebookId: userInFacebook.id,
//         password: password
//       })
//       user = await user.save()
//       await Email.sendOauthRegistrationEmail({ email: user.email, password: password })
//     }
//     const jwt = makeJwt(user)
//     res.status(200).json({ ok: true, token: jwt }).cookie('token', jwt, { maxAge: MaxAgeForCookieJWT }).redirect('/socialsAuth')
//   } catch (error) {
//     console.error(error)
//     res.status(500).json({ ok: false, token: null })
//   }
// })

// /api/auth/email/verify/:emailToken
router.get('/email/verify/:emailToken', async (req, res) => {
  try {
    let user = await User.findOne({ emailToken: req.params.emailToken })
    if (user) {
      user.emailVerified = true
      await user.save()
      res.redirect('/signin')
    } else {
      res.send('Wrong verification code')
    }
  } catch (error) {
    res.redirect(config.server.hostname)
  }
})

module.exports = router