const mongoose = require('mongoose')
const config = require('../config')

mongoose.Promise = global.Promise

const options = {
  useMongoClient: true,
  keepAlive: 300000,
  connectTimeoutMS: 30000,
  reconnectInterval: 2000,
  reconnectTries: Number.MAX_VALUE
}

const dbUri = config.server.mongo.url

mongoose.connect(dbUri, options)

mongoose.connection.on('connected', onDatabaseConnection)
mongoose.connection.on('disconnected', onDatabaseDisconnection)
mongoose.connection.on('error', onDatabaseError)

function onDatabaseConnection() {
  console.log('Database connection is open!')
}

function onDatabaseDisconnection() {
  console.log('Database connection is lost')
  mongoose.connect(dbUri, options)
}

function onDatabaseError(err) {
  console.log(`Database connection has an error: ${err}`)
  mongoose.disconnect()
}

module.exports = mongoose.connection
