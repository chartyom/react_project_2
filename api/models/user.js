const mongoose = require('mongoose')
const bcrypt = require('bcryptjs')
const SALT_WORK_FACTOR = 10

const Schema = mongoose.Schema

const userModel = new Schema(
  {
    email: { type: String, required: true },
    emailVerified: { type: Boolean, default: false },
    emailToken: { type: String },
    password: { type: String, required: true },
    ethWallet: { type: String },
    etcWallet: { type: String },
    btcWallet: { type: String },
    bchWallet: { type: String },
    dashWallet: { type: String },
    ltcWallet: { type: String },
    country: { type: String, required: true },
    firstName: { type: String, default: '' },
    lastName: { type: String, default: '' },
    __v: { type: Number, select: false }
  },
  {
    timestamps: false
  }
)

userModel.pre('save', function (next) {
  var user = this

  if (!user.isModified('password')) return next()

  bcrypt.genSalt(SALT_WORK_FACTOR, function (err, salt) {
    if (err) return next(err)

    bcrypt.hash(user.password, salt, function (err, hash) {
      if (err) return next(err)

      user.password = hash
      next()
    })
  })
})

userModel.methods.comparePassword = async function (candidatePassword) {
  try {
    let isMatch = await bcrypt.compare(candidatePassword, this.password)
    return isMatch
  } catch (error) {
    return Promise.reject(error)
  }
}

module.exports = mongoose.model('User', userModel)
