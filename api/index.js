const router = require('express').Router()
require('./mongo')
require('./email')

// /api/auth
router.use('/auth', require('./routes/auth'))

// /api/user
router.use('/user', require('./routes/user'))
router.get('/test', (req, res) => { res.send(1) })

module.exports = router