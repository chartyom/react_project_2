const config = require('../config')
const transporter = require('nodemailer').createTransport(config.email)

// verify connection configuration
transporter.verify(function (error, success) {
  if (error) {
    console.log(error);
  } else {
    console.log('Email server is ready');
  }
})

async function sendRegistrationEmail({ email, emailToken }) {
  return await transporter.sendMail({
    from: `"Bitkom.io" <${config.email.auth.user}>`,
    subject: 'Email confirmation',
    to: email,
    html: `Please verify your email by clicking this link <a href='${config.server.hostname}api/auth/email/verify/${emailToken}'>Verify email</a>`
  })
}

async function sendOauthRegistrationEmail({ email, password }) {
  return await transporter.sendMail({
    from: `"Bitkom.io" <${config.email.auth.user}>`,
    subject: 'Successful registration',
    to: email,
    html: `Account was created with facebook (${email}) in <a href='${config.server.hostname}'>bitkom.io</a>. You can sign in with facebook button or with password. Password for you is '${password}'`
  })
}

// TODO
async function sendDublicateEmail({ email }) {
  return await transporter.sendMail({
    from: `"Bitkom.io" <${config.email.auth.user}>`,
    subject: 'Successful registration',
    to: email,
    html: `Account already exists with this email: ${email}. Try to sign in <a href='${config.server.hostname}/signIn'>bitkom.io</a>.`
  })
}

module.exports = {
  sendRegistrationEmail,
  sendOauthRegistrationEmail,
  sendDublicateEmail
}
