const jwt = require('jsonwebtoken')
const config = require('../config')

function checkToken (req, res, next) {
  let tokenCookie = req.cookies.token
  let tokenHeader = req.headers['Authorization'] || req.headers['authorization']

  if (!tokenCookie && !tokenHeader) {
    res.status(401).json({ ok: false, message: 'Empty cookie and header' })
    return
  }

  let token = tokenCookie || tokenHeader

  let data = jwt.verify(token, config.secret)
  if (data.id && data.email) {
    req.user = data
    next()
  } else {
    res.status(401).json({ ok: false, message: 'Wrong token' })
  }
}

function timelineRule (req, res, next) {
  let now = new Date()
  let nowUtcInSec = Number((now.getTime() / 1000).toFixed(0))
  if (nowUtcInSec < config.bitkomwalletsPublishDate) {  // change to testBitkomwalletsPublishDate for testing
    res.status(500).json({ ok: false, message: 'Too early access' })
  } else {
    next()
  }
}

module.exports = {
  checkToken,
  timelineRule
}